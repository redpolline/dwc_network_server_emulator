#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2014 msoucy
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import json
import logging
import time
import urlparse
import BaseHTTPServer
import SocketServer
import threading
import os
import sys
import random
import traceback
import configparser

from gamespy.gs_database_nas import NASGamespyDatabase as NASGamespyDatabase
import gamespy.gs_utility as gs_utils
import other.utils as utils

# Config settings.
config_file = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), "./config.ini"))
config_sect = "NasServer"

# Logger settings
logger_name = "NasServer"
logger = utils.create_logger(config_file, config_sect, logger_name)

# if a game from this list requests a file listing, the server will return that only one exists and return a random one
# this is used for Mystery Gift distribution on Generation 4 Pokemon games
gamecodes_return_random_file = ['ADAD', 'ADAE', 'ADAF', 'ADAI', 'ADAJ', 'ADAK', 'ADAS', 'CPUD', 'CPUE', 'CPUF', 'CPUI', 'CPUJ', 'CPUK', 'CPUS', 'IPGD', 'IPGE', 'IPGF', 'IPGI', 'IPGJ', 'IPGK', 'IPGS']

#address = ("0.0.0.0", 80)

class NasServer(object):
    def start(self):
        global config_sect
        address = ["", 9000]

        # Get configuration.
        config = configparser.ConfigParser()
        try:
            config.read(config_file)

            # Get optional sections.
            if config.has_section(config_sect) == True:
                if config.has_option(config_sect, "server_address") == True:
                    address[0] = utils.get_ip_from_config(config.get(config_sect, "server_address"))
                    if (address[0] == "0.0.0.0"):
                        raise Exception("Invalid server_address in config!")

            # Set defaults.
            if len(address[0]) == 0:
                address[0] = utils.get_ip_from_config("", False)

        except Exception as e:
            if logger is not None:
                logger.log(logging.ERROR, "Could not parse config section [%s]. Exception raised: [%s]", config_sect, e)
            sys.exit("Could not parse config.ini section [%s]. Exception Raised: [%s]" % (config_sect, e))

        httpd = NasHTTPServer((address[0], address[1]), NasHTTPServerHandler)
        t = threading.Thread(target=httpd.serve_forever)
        t.daemon = True
        t.start()
        logger.log(logging.INFO, "Now listening for connections on %s:%d...", address[0], address[1])
        httpd.serve_forever()

class NasHTTPServer(SocketServer.ThreadingMixIn,BaseHTTPServer.HTTPServer):
    def __init__(self, server_address, RequestHandlerClass):
        self.address = server_address
        self.db = NASGamespyDatabase("GamespyDatabase [%s]" % logger_name)
        self.db.register_nas_server(self.address[0], self.address[1])
        BaseHTTPServer.HTTPServer.__init__(self, self.address, RequestHandlerClass)

    def __del__(self):
        self.close()
        super.__del__()

    def close(self):
        if self.address != None and self.address[0] > 0 and self.address[1] > 1:
            self.db.unregister_nas_server(self.address[0], self.address[1])

        self.db.close()

class NasHTTPServerHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def version_string(self):
        return "Nintendo Wii (http)"

    def do_GET(self):
        try:
            # conntest server
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.send_header("X-Organization", "Nintendo")
            self.send_header("Server", "BigIP")
            self.end_headers()
            self.wfile.write("ok")
        except:
            logger.log(logging.ERROR, "Unknown exception: %s" % traceback.format_exc())

    def do_POST(self):
# Error Codes:
#
#    Code:       Meaning:
#    0103        Console Identifier broken. (Invalid request without valid console ids.)
#    0104        Console ID already in use. (Will cause the client to generate a new userid if in acctcreate.)
#    0105        [Unknown] - (Wifimii says it's Console Identifier doesn't exist on server.)
#    0108        User ID deleted due to inactivity.
#

        try:
            length = int(self.headers['content-length'])
            post = self.str_to_dict(self.rfile.read(length))
            if self.client_address[0] == '127.0.0.1':
                client_address = (self.headers.get('x-forwarded-for', self.client_address[0]), self.client_address[1])
            else:
                client_address = self.client_address

            post['ipaddr'] = client_address[0]

            if self.path == "/ac":
                logger.log(logging.DEBUG, "Request to %s from %s", self.path, client_address)
                logger.log(logging.DEBUG, post)
                ret = {
                    "datetime": time.strftime("%Y%m%d%H%M%S"),
                    "retry": "0"
                }
                action = "INVALID"
                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.send_header("NODE", "wifiappe1")

                # Attempt to detect the client type.
                # 0: Wii
                # 1: Everything else.
                client_type = 0

                # Basic check for console ids.
                invalid_req = 0
                ultimate_failure = 0
                try:
                    if 'macadr' not in post or len(str(post['macadr'])) <= 0:
                        logger.log(logging.DEBUG, "/ac: Missing macadr in request. "+str(post))
                        invalid_req = 1
                    elif 'action' not in post or len(str(post['action'])) <= 0:
                        logger.log(logging.DEBUG, "/ac: Missing action in request. "+str(post))
                        invalid_req = 1
                    elif 'ipaddr' not in post or len(str(post['ipaddr'])) <= 0:
                        logger.log(logging.DEBUG, "/ac: Missing ipaddr in request. "+str(post))
                        invalid_req = 1
                    elif 'gamecd' not in post or len(str(post['gamecd'])) <= 0:
                        logger.log(logging.DEBUG, "/ac: Missing console gamecd in request. "+str(post))
                        invalid_req = 1
                    elif 'gsbrcd' in post and str(post['gsbrcd']) == self.server.db.get_failure():
                        logger.log(logging.DEBUG, "/ac: Failure gsbrcd in request. "+str(post))
                        invalid_req = 1
                        ultimate_failure = 1
                    elif 'userid' not in post or len(str(post['userid'])) <= 0:
                        if post['action'] != 'acctcreate':
                            logger.log(logging.DEBUG, "/ac: Missing userid in request. "+str(post))
                            invalid_req = 1
                except (KeyError, NameError) as e:
                    logger.log(logging.DEBUG, "/ac: Missing console identifiers in request. E: "+str(e)+" Given Params: "+str(post))
                    invalid_req = 1

                if invalid_req == 0:
                    try:
                        if len(str(post['csnum'])) <= 0:
                            logger.log(logging.DEBUG, "/ac: Invalid csnum in request. "+str(post))
                            invalid_req = 1
                    except (KeyError, NameError):
                            invalid_req = 2

                    # Check for the NDS / "other" consoles.
                    if invalid_req == 2:
                        try:
                            if 'passwd' not in post or len(str(post['passwd'])) <= 0:
                                logger.log(logging.DEBUG, "/ac: Invalid passwd in request. "+str(post))
                                invalid_req = 1
                            elif 'bssid' not in post or len(str(post['bssid'])) <= 0:
                                logger.log(logging.DEBUG, "/ac: Invalid bssid in request. "+str(post))
                                invalid_req = 1
                            else:
                                # We're good here.
                                client_type = 1
                                invalid_req = 0
                        except (KeyError, NameError):
                            logger.log(logging.DEBUG, "/ac: Missing console csnum / passwd + bssid in request. "+str(post))
                            invalid_req = 1

                if invalid_req == 1:
                    ret = {
                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                        "returncd": "0103",
                        "locator": "gamespy.com",
                        "retry": "1",
                        "reason": "Invalid request."
                    }
                    if ultimate_failure == 1:
                        ret["reason"] = "Invalid request. Sent failure. Received failure."
                else:
                    action = post['action']
                    del invalid_req
                    del ultimate_failure

                if action == "acctcreate":
                    if self.server.db.is_ip_banned(post):
                        logger.log(logging.DEBUG, "acctcreate denied for banned user "+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3913",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User banned."
                        }
                    elif not self.server.db.valid_mac(post):
                        logger.log(logging.DEBUG, "acctcreate denied because MAC is not a valid length"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.is_console_macadr_banned(post):
                        logger.log(logging.DEBUG, "acctcreate denied for banned console"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3914",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User's console is banned."
                        }
                    elif self.server.db.console_abuse(post):
                        logger.log(logging.DEBUG, "acctcreate denied - Console is banned due to abuse of identifiers"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif not self.server.db.console_register(post):
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "2222",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.pending_console(post):
                        logger.log(logging.DEBUG, "Console pending manual activation"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3000",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    else:
                        if 'userid' in post and post["userid"] is not None:
                            if self.server.db.check_user_exists_uid(post["userid"]) == True:
                                ret = {
                                    "returncd": "002",
                                    "userid": post["userid"]
                                }
                            else:
                                # Check if the client generated ID is already taken.
                                if self.server.db.check_client_generated_user_id_exists(post["userid"]) == False:
                                    # Need to create the user.
                                    userid = self.server.db.create_initial_user(post)

                                    # Userids cannot be greater than 9 chars, or some NDS games (like Pokemon Gen4)
                                    # will suffer memory corruption when parsing the /lc/2 response.
                                    if len(str(userid)) <= 9:
                                        ret = {
                                            "returncd": "002",
                                            "userid": userid
                                        }
                                    else:
                                        ret = {
                                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                                            "returncd": "999",
                                            "locator": "gamespy.com",
                                            "retry": "1",
                                            "reason": "No available Userid."
                                        }
                                else:
                                    # Check if this is the same client.
                                    if self.server.db.check_same_userid_from_generated_user_id(post["userid"], post["macadr"]) == True:
                                        # Same client.
                                        ret = {
                                            "returncd": "002",
                                            "userid": self.server.db.get_userid_from_generated_user_id(post["userid"])
                                        }
                                    else:
                                        # Generated client userid is taken by another client, have client generate a new one.
                                        ret = {
                                            "returncd": "0104",
                                            "locator": "gamespy.com",
                                            "userid": post['userid'],
                                            "retry": "1",
                                            "reason": "Userid already taken!"
                                        }
                        else:
                            if client_type == 0:
                                # The wii might not send a userid on acctcreate.
                                # If so, we need to generate one.
                                temp_userid = 0
                                while temp_userid == 0:
                                    temp_userid = random.randint(0, 9999999999999)
                                    if self.server.db.check_client_generated_user_id_exists(temp_userid):
                                        temp_userid = 0
                                post['userid'] = temp_userid

                                # Now create the user.
                                userid = self.server.db.create_initial_user(post)

                                # Userids cannot be greater than 9 chars, or some NDS games (like Pokemon Gen4)
                                # will suffer memory corruption when parsing the /lc/2 response.
                                if len(str(userid)) <= 9:
                                    ret = {
                                        "returncd": "002",
                                        # The wii will accept and use whatever we return here, unlike the NDS.
                                        "userid": str(temp_userid)
                                    }
                                else:
                                    ret = {
                                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                                        "returncd": "999",
                                        "locator": "gamespy.com",
                                        "retry": "1",
                                        "reason": "No available Userid."
                                    }
                            else:
                                logger.log(logging.WARNING, "acctcreate userid is None!")
                                ret = {
                                    "returncd": "0108",
                                    "locator": "gamespy.com",
                                    "retry": "1",
                                    "reason": "Missing userid!"
                                }
                    logger.log(logging.DEBUG, "acctcreate response to %s", client_address)
                    logger.log(logging.DEBUG, ret)

                    ret = self.dict_to_str(ret)

                elif action == "login":
                    if self.server.db.is_ap_banned(post):
                        logger.log(logging.DEBUG, "login denied for banned user (AP is banned) "+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3917",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User banned."
                        }
                    elif self.server.db.is_ip_banned(post):
                        logger.log(logging.DEBUG, "login denied for banned user (IP is banned) "+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3917",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User banned."
                        }
                    elif not self.server.db.valid_mac(post):
                        logger.log(logging.DEBUG, "Login denied because MAC is not a valid length"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.is_console_macadr_banned(post):
                        logger.log(logging.DEBUG, "login denied for banned console"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3914",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User's console is banned."
                        }
                    elif self.server.db.console_abuse(post):
                        logger.log(logging.DEBUG, "Login denied - Console is banned due to abuse of identifiers"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif not self.server.db.console_register(post):
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "2222",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.pending_console(post):
                        logger.log(logging.DEBUG, "Console pending manual activation"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3000",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif not self.server.db.allowed_games(post):
                        logger.log(logging.DEBUG, "Login denied - game not supported!"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3800",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    else:
                        uid = self.server.db.get_userid_from_generated_user_id(post["userid"])
                        if uid == 0:
                            # Note: We send 0108, because that will cause the client to begin an acctcreate action.
                            logger.log(logging.DEBUG, "Login denied because the user doesn't exist. "+str(post))
                            ret = {
                                "datetime": time.strftime("%Y%m%d%H%M%S"),
                                "returncd": "0108",
                                "locator": "gamespy.com",
                                "retry": "1",
                            }
                        else:
                            if self.server.db.is_user_banned(uid):
                                ret = {
                                    "datetime": time.strftime("%Y%m%d%H%M%S"),
                                    "returncd": "3912",
                                    "locator": "gamespy.com",
                                    "retry": "1",
                                    "reason": "User is banned."
                                }
                            else:
                                pid = self.server.db.create_initial_profile(post, uid) # Returns profileid, if profile already exists.
                                if pid == 0:
                                    logger.log(logging.DEBUG, "Login denied because we couldn't find nor create a user profile. "+str(post))
                                    ret = {
                                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                                        "returncd": "998",
                                        "locator": "gamespy.com",
                                        "retry": "1"
                                    }
                                elif self.server.db.is_profile_banned(pid):
                                    logger.log(logging.DEBUG, "login denied for banned profile"+str(post))
                                    ret = {
                                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                                        "returncd": "3912",
                                        "locator": "gamespy.com",
                                        "retry": "1",
                                        "reason": "User's profile is banned."
                                    }
                                else:
                                    challenge = utils.generate_random_str(8)
                                    post["challenge"] = challenge
                                    authtoken = self.server.db.generate_authtoken(uid,post)
                                    if authtoken is not None:
                                        ret.update({
                                            "returncd": "001",
                                            "locator": "gamespy.com",
                                            "challenge": challenge,
                                            "token": authtoken,
                                            "userid": uid
                                        })
                                    else:
                                        ret.update({
                                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                                            "returncd": "997",
                                            "locator": "gamespy.com",
                                            "retry": "1",
                                            "reason": "Login failure."
                                        })

                    logger.log(logging.DEBUG, "login response to %s", client_address)
                    logger.log(logging.DEBUG, ret)

                    ret = self.dict_to_str(ret)

                elif action == "SVCLOC" or action == "svcloc": # Get service based on service id number
                    ret["returncd"] = "007"
                    ret["statusdata"] = "Y"
                    if self.server.db.is_ap_banned(post):
                        logger.log(logging.DEBUG, "svcloc denied for banned user (AP is banned) "+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3917",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User banned."
                        }
                    elif self.server.db.is_ip_banned(post):
                        logger.log(logging.DEBUG, "svcloc denied for banned user (IP is banned) "+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3917",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User banned."
                        }
                    elif not self.server.db.valid_mac(post):
                        logger.log(logging.DEBUG, "svcloc denied because MAC is not a valid length"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.is_console_macadr_banned(post):
                        logger.log(logging.DEBUG, "svcloc denied for banned console"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3914",
                            "locator": "gamespy.com",
                            "retry": "1",
                            "reason": "User's console is banned."
                        }
                    elif self.server.db.console_abuse(post):
                        logger.log(logging.DEBUG, "svcloc denied - Console is banned due to abuse of identifiers"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3915",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif not self.server.db.console_register(post):
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "2222",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif self.server.db.pending_console(post):
                        logger.log(logging.DEBUG, "Console pending manual activation"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3000",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    elif not self.server.db.allowed_games(post):
                        logger.log(logging.DEBUG, "Login denied - game not supported!"+str(post))
                        ret = {
                            "datetime": time.strftime("%Y%m%d%H%M%S"),
                            "returncd": "3800",
                            "locator": "gamespy.com",
                            "retry": "1",
                        }
                    else:
                        uid = self.server.db.get_userid_from_generated_user_id(post["userid"])
                        if uid == 0:
                            # Note: We send 0108, because that will cause the client to begin an acctcreate action.
                            logger.log(logging.DEBUG, "svcloc denied because the user doesn't exist. "+str(post))
                            ret = {
                                "datetime": time.strftime("%Y%m%d%H%M%S"),
                                "returncd": "0108",
                                "locator": "gamespy.com",
                                "retry": "1",
                            }
                        else:
                            if self.server.db.is_user_banned(uid):
                                ret = {
                                    "datetime": time.strftime("%Y%m%d%H%M%S"),
                                    "returncd": "3912",
                                    "locator": "gamespy.com",
                                    "retry": "1",
                                    "reason": "User is banned."
                                }
                            else:
                                pid = self.server.db.create_initial_profile(post, uid) # Returns profileid, if profile already exists.
                                if pid == 0:
                                    logger.log(logging.DEBUG, "Login denied because we couldn't find nor create a user profile. "+str(post))
                                    ret = {
                                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                                        "returncd": "998",
                                        "locator": "gamespy.com",
                                        "retry": "1"
                                    }
                                elif self.server.db.is_profile_banned(pid):
                                    logger.log(logging.DEBUG, "login denied for banned profile"+str(post))
                                    ret = {
                                        "datetime": time.strftime("%Y%m%d%H%M%S"),
                                        "returncd": "3912",
                                        "locator": "gamespy.com",
                                        "retry": "1",
                                        "reason": "User's profile is banned."
                                    }
                                else:
                                    authtoken = self.server.db.generate_authtoken(uid, post)

                                    if 'svc' in post:
                                        logger.log(logging.DEBUG, "svcloc request recieved for svc ID [%s].", str(svc))
                                        if post["svc"] == "0000": # Pokemon requests this for some things
                                            ret["servicetoken"] = authtoken
                                            ret["svchost"] = "n/a"
                                        else:
                                            if post["svc"] in ("9000", "9001"): # DLC host = 9000
                                                ret["svchost"] = self.headers['host'] # in case the client's DNS isn't redirecting dls1.nintendowifi.net

                                                # Brawl has 2 host headers which Apache chokes on, so only return the first one or else it won't work
                                                ret["svchost"] = ret["svchost"].split(',')[0]

                                                if post["svc"] == 9000:
                                                    ret["token"] = authtoken
                                                else:
                                                    ret["servicetoken"] = authtoken
                                            else:
                                                logger.log(logging.WARNING, "svcloc request recieved for unknown svc ID [%s].", str(svc))
                                    else:
                                        logger.log(logging.DEBUG, "Invalid svcloc request recieved. No svc ID was sent by the client.")

                    logger.log(logging.DEBUG, "svcloc response to %s", client_address)
                    logger.log(logging.DEBUG, ret)

                    ret = self.dict_to_str(ret)
                else:
                    logger.log(logging.WARNING, "Unknown action request %s from %s!", self.path, client_address)


            elif self.path == "/pr":
                logger.log(logging.DEBUG, "Request to %s from %s", self.path, client_address)
                logger.log(logging.DEBUG, post)
                words = len(post["words"].split('\t'))
                wordsret = "0" * words
                ret = {
                    "prwords": wordsret,
                    "returncd": "000",
                    "datetime": time.strftime("%Y%m%d%H%M%S")
                }

                for l in "ACEJKP":
                    ret["prwords"+l] = wordsret

                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.send_header("NODE", "wifiappe1")

                logger.log(logging.DEBUG, "pr response to %s", client_address)
                logger.log(logging.DEBUG, ret)

                ret = self.dict_to_str(ret)

            elif self.path == "/download":
                logger.log(logging.DEBUG, "Request to %s from %s", self.path, client_address)
                logger.log(logging.DEBUG, post)

                action = post["action"]

                ret = ""
                dlcdir = os.path.abspath('dlc')
                dlcpath = os.path.abspath("dlc/" + post["gamecd"])
                dlc_contenttype = False

                if os.path.commonprefix([dlcdir, dlcpath]) != dlcdir:
                    logging.log(logging.WARNING, 'Attempted directory traversal attack "%s", cancelling.', dlcpath)
                    self.send_response(403)
                    return

                def safeloadfi(fn, mode='rb'):
                    '''
                    safeloadfi : string -> string

                    Safely load contents of a file, given a filename, and closing the file afterward
                    '''
                    with open(os.path.join(dlcpath, fn), mode) as fi:
                        return fi.read()

                if action == "count":
                    if post["gamecd"] in gamecodes_return_random_file:
                        ret = "1"
                    else:
                        count = 0

                        if os.path.exists(dlcpath):
                            count = len(os.listdir(dlcpath))

                            if os.path.isfile(dlcpath + "/_list.txt"):
                                attr1 = post.get("attr1", None)
                                attr2 = post.get("attr2", None)
                                attr3 = post.get("attr3", None)

                                dlcfi = safeloadfi("_list.txt")
                                lst = self.filter_list(dlcfi, attr1, attr2, attr3)
                                count = self.get_file_count(lst)

                        ret = "%d" % count

                if action == "list":
                    num = post.get("num", None)
                    offset = post.get("offset", None)

                    if num != None:
                        num = int(num)

                    if offset != None:
                        offset = int(offset)

                    attr1 = post.get("attr1", None)
                    attr2 = post.get("attr2", None)
                    attr3 = post.get("attr3", None)

                    if os.path.exists(dlcpath):
                        # Look for a list file first.
                        # If the list file exists, send the entire thing back to the client.
                        if os.path.isfile(os.path.join(dlcpath, "_list.txt")):
                            if post["gamecd"].startswith("IRA") and attr1.startswith("MYSTERY"):
                                # Pokemon BW Mystery Gifts, until we have a better solution for that
                                ret = self.filter_list(safeloadfi("_list.txt"), attr1, attr2, attr3)
                                ret = self.filter_list_g5_mystery_gift(ret, post["rhgamecd"])
                                ret = self.filter_list_by_date(ret, post["token"])
                            elif post["gamecd"] in gamecodes_return_random_file:
                                # Pokemon Gen 4 Mystery Gifts, same here
                                ret = self.filter_list(safeloadfi("_list.txt"), attr1, attr2, attr3)
                                ret = self.filter_list_by_date(ret, post["token"])
                            else:
                                # default case for most games
                                ret = self.filter_list(safeloadfi("_list.txt"), attr1, attr2, attr3, num, offset)

                if action == "contents":
                    # Get only the base filename just in case there is a path involved somewhere in the filename string.
                    dlc_contenttype = True
                    contents = os.path.basename(post["contents"])
                    ret = safeloadfi(contents)

                self.send_response(200)

                if dlc_contenttype == True:
                    self.send_header("Content-type", "application/x-dsdl")
                    self.send_header("Content-Disposition", "attachment; filename=\"" + post["contents"] + "\"")
                else:
                    self.send_header("Content-type", "text/plain")

                self.send_header("X-DLS-Host", "http://127.0.0.1/")

                logger.log(logging.DEBUG, "download response to %s", client_address)

                #if dlc_contenttype == False:
                #    logger.log(logging.DEBUG, ret)
            else:
                self.send_response(404)
                logger.log(logging.WARNING, "Unknown path request %s from %s!", self.path, client_address)
                return

            self.send_header("Content-Length", str(len(ret)))
            self.end_headers()
            self.wfile.write(ret)
        except:
            logger.log(logging.ERROR, "Unknown exception: %s" % traceback.format_exc())

    def str_to_dict(self, s):
        ret = urlparse.parse_qs(s)

        for k, v in ret.iteritems():
            try:
                # I'm not sure about the replacement for '-', but it'll at least let it be decoded.
                # For the most part it's not important since it's mostly used for the devname/ingamesn fields.
                ret[k] = base64.b64decode( urlparse.unquote( v[0] ).replace("*", "=").replace("?", "/").replace(">","+").replace("-","/") )
            except TypeError:
                print "Could not decode following string: ret[%s] = %s" % (k, v[0])
                print "url: %s" % s
                ret[k] = v[0] # If you don't assign it like this it'll be a list, which breaks other code.

        return ret

    def dict_to_str(self, dict):
        for k, v in dict.iteritems():
            dict[k] = base64.b64encode(v).replace("=", "*")

        # nas(wii).nintendowifi.net has a URL query-like format but does not use encoding for special characters
        return "&".join("{!s}={!s}".format(k, v) for k, v in dict.items()) + "\r\n"
    
    # custom selection for generation 5 mystery gifts, so that the random or data-based selection still works properly
    def filter_list_g5_mystery_gift(self, data, rhgamecd):
        if rhgamecd[2] == 'A':
            filterBit = 0x100000
        elif rhgamecd[2] == 'B':
            filterBit = 0x200000
        elif rhgamecd[2] == 'D':
            filterBit = 0x400000
        elif rhgamecd[2] == 'E':
            filterBit = 0x800000
        else:
            # unknown game, can't filter
            return data
        
        output = []
        for line in data.splitlines():
            lineBits = int(line.split('\t')[3], 16)
            if lineBits & filterBit == filterBit:
                output.append(line)
        return '\r\n'.join(output) + '\r\n'

    def filter_list_by_date(self, data, token):
        # allow user to control which file to receive by setting the local date
        # selected file will be the one at index (day of year) mod (file count)
        try:
            userData = self.server.db.get_nas_login(token)
            date = time.strptime(userData['devtime'], '%y%m%d%H%M%S')
            files = data.splitlines()
            ret = files[(int(date.tm_yday) - 1) % len(files)] + '\r\n'
        except:
            ret = self.filter_list_random_files(data, 1)
        return ret
        
    def filter_list_random_files(self, data, count):
        # Get [count] random files from the filelist
        samples = random.sample(data.splitlines(), count)
        return '\r\n'.join(samples) + '\r\n'

    def filter_list(self, data, attr1 = None, attr2 = None, attr3 = None, num = None, offset = None):
        if attr1 == None and attr2 == None and attr3 == None and num == None and offset == None:
            # Nothing to filter, just return the input data
            return data

        # Filter the list based on the attribute fields
        nc = lambda a, b: (a is None or a == b)
        attrs = lambda data: (len(data) == 6 and nc(attr1, data[2]) and nc(attr2, data[3]) and nc(attr3, data[4]))
        output = filter(lambda line: attrs(line.split("\t")), data.splitlines())

        if offset != None:
            output = output[offset:]

        if num != None:
            output = output[:num]

        # if nothing matches, at least return a newline; Pokemon BW at least expects this and will error without it
        return '\r\n'.join(output) + '\r\n'

    def get_file_count(self, data):
        return sum(1 for line in data.splitlines() if line)

if __name__ == "__main__":
    nas = NasServer()
    nas.start()
