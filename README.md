dwc_network_server_emulator   
===========================

A Nintendo DS and Wii online multiplayer server emulator.

## How To Install ?
1) Clone / Extract the project to your intended installation directory (I.e. Your DocumentRoot.)

2) If you don't have a mysql server ready to use, set it up now. (Google is your friend.)

3) Rename config.ini.example to config.ini, and fill out the required database connection information. (Hostname / User name / password / database)

4) Create a secure database salt and write it into config.ini. This is used to protect passwords in the mysql database, so make sure to follow best practices when creating the database salt. (Google is your friend.)

5) As config.ini contains secrets, ensure that only the web server has access to it. (I.e. Disallow read access to anyone that isn't the webserver itself via your OS's filesystem permissions.)

6) Run init_db.py in the installation directory. If it prints errors, you'll need to double check your config.ini settings.

7) Run master_server.py.

If you want your server to automatically run master_server.py on boot, or automatically restart master_server.py after a crash, you'll need to create a service for your OS. (Google is your friend. Hint: [Windows](https://stackoverflow.com/questions/32404/how-do-you-run-a-python-script-as-a-service-in-windows) [Linux](https://wiki.debian.org/systemd/Services) [Linux again](https://wiki.archlinux.org/title/Systemd))

## Old Install Instructions Below:

Instructions for setting up your own server can be found [here](https://github.com/EnergyCube/dwc_network_server_emulator/wiki) (and the old instructions from the original project [here](https://github.com/polaris-/dwc_network_server_emulator/wiki/Setting-up-a-server-from-a-fresh-installation-of-Linux)).

Help : [Usage Instructions](https://web.archive.org/web/20180613062128/https://github.com/polaris-/dwc_network_server_emulator/wiki) | [Partial Compatibilty List](https://web.archive.org/web/20180613062128/https://github.com/polaris-/dwc_network_server_emulator/wiki/Compatibility) | [Available Content](https://web.archive.org/web/20180613062128/https://github.com/polaris-/dwc_network_server_emulator/wiki/Nintendo-DS-Download-Content)

## WANTED: More programmers!
Whether it's to add new features or clean up existing code, we could always use the additional help. Check the bug tracker or ask another developers in #altwfc @ irc.rizon.net to see what bugs need to be fixed.  

Open source projects referenced during the creation of this project: [OpenSpy Core](https://github.com/sfcspanky/Openspy-Core/) | [Luigi Auriemma's Gslist and enctypex_decoder](http://aluigi.altervista.org/papers.htm)
