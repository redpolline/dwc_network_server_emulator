#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import mysql.connector
from mysql.connector import Error
import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils

#class PortSqliteRow(collections.abc.Mapping):
class PortSqliteRow(collections.Mapping, collections.Iterator):
    def __init__(self, keys, row):
        self.row_data = row
        self.row_keys = keys
        self.index = 0

    def __iter__(self):
         for d in self.row_data:
             yield d
#        return self

    def __next__(self):
        if self.index < len(self.row_keys):
            old_index = self.index
            self.index = self.index + 1
            return self.row_keys[old_index]
        else:
            raise StopIteration

    def __contains__(self, item):
        for i in range(len(self.row_keys)):
            if self.row_keys[i] == item:
                return True

        return False

    def __len__(self):
        return len(self.row_keys)

    def keys(self):
        return list(self.row_keys)

    def values(self):
        return list(self.row_data)

    def next(self):
        return self.__next__()

    def __getitem__(self, item):
        if isinstance(item, (int, long)):
            if item < len(self.row_data):
                return self.row_data[item]
            else:
                return None
        else:
            for i in range(len(self.row_keys)):
                if self.row_keys[i] == item:
                    if i < len(self.row_data):
                        return self.row_data[i]
                    else:
                        return None

        raise KeyError

    def __eq__(self, other):
        equal = False
        if len(self.row_data) == len(other.row_data) and len(self.row_keys) == len(other.row_keys):
            equal = True
            for i in range(len(self.row_data)):
                if self.row_data[i] != other.row_data[i]:
                    equal = False
                    break

            if equal == True:
                for i in range(len(self.row_keys)):
                    if self.row_keys[i] != other.row_keys[i]:
                        equal = False
                        break

        return equal

    def __ne__(self, other):
        return self.equal(other) == False

class Transaction(object):
    def __init__(self, connection, logger):
        self.conn = connection
        self.logger = logger
        self.databaseAltered = False

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if self.databaseAltered:
            self.conn.commit()
        return

    def _executeAndMeasure(self, cursor, statement, parameters=None):
        logTransactionId = utils.generate_random_str(8)

        if parameters is not None:
            self.logger.log(logging.DEBUG, "[%s] STARTING: " % logTransactionId + statement.replace('?', '%s') % parameters)
        else:
            self.logger.log(logging.DEBUG, "[%s] STARTING: " % logTransactionId + statement)

        timeStart = time.time()
        clockStart = time.clock()

        cursor.execute(statement, parameters)

        clockEnd = time.clock()
        timeEnd = time.time()
        timeDiff = timeEnd - timeStart

        self.logger.log(logging.DEBUG, "[%s] DONE: Took %s real time / %s processor time", logTransactionId, timeDiff, clockEnd - clockStart)
        if timeDiff > 1.0:
            self.logger.log(logging.WARNING, "[%s] WARNING: SQL Statement took %s seconds!", logTransactionId, timeDiff)
            self.logger.log(logging.WARNING, "[%s] " % logTransactionId + statement.replace('?', '%s') % parameters)
        return

    def _get_keys(self, cursor):
        keys = []
        for i in range(len(cursor.description)):
            desc = cursor.description[i]
            keys.append(desc[0])
        return keys

    def queryall(self, statement, parameters=()):
        with closing(self.conn.cursor()) as cursor:
            self._executeAndMeasure(cursor, statement, parameters)
            rows = cursor.fetchall()
            if rows is not None and len(rows) > 0:
                ret = []
                for row in rows:
                    keys = self._get_keys(cursor)
                    ret.append(PortSqliteRow(keys, row))

                return ret

        return []

    def queryone(self, statement, parameters=()):
        with closing(self.conn.cursor()) as cursor:
            self._executeAndMeasure(cursor, statement, parameters)
            row = cursor.fetchone()
            if row is not None and len(row) > 0:
                keys = self._get_keys(cursor)
                ret = PortSqliteRow(keys, row)
                row = cursor.fetchone()
                while row is not None:
                    self.logger.log(logging.DEBUG, "Discarding result: "+str(row))
                    row = cursor.fetchone()
                return ret
        return []

    def queryall_raw(self, statement, parameters=()):
        with closing(self.conn.cursor(raw=True)) as cursor:
            self._executeAndMeasure(cursor, statement, parameters)
            rows = cursor.fetchall()
            if rows is not None and len(rows) > 0:
                ret = []
                for row in rows:
                    keys = self._get_keys(cursor)
                    ret.append(PortSqliteRow(keys, row))

                return ret

        return []

    def queryone_raw(self, statement, parameters=()):
        with closing(self.conn.cursor(raw=True)) as cursor:
            self._executeAndMeasure(cursor, statement, parameters)
            row = cursor.fetchone()
            if row is not None and len(row) > 0:
                keys = self._get_keys(cursor)
                ret = PortSqliteRow(keys, row)
                row = cursor.fetchone()
                while row is not None:
                    self.logger.log(logging.DEBUG, "Discarding result: "+str(row))
                    row = cursor.fetchone()
                return ret
        return []

    def nonquery(self, statement, parameters=()):
        with closing(self.conn.cursor()) as cursor:
            self._executeAndMeasure(cursor, statement, parameters)
            self.databaseAltered = True
        return

class GamespyDatabase(object):
    def __init__(self, logname=None, configsect=None):
        self.conn = None
        self.dbsalt = None
        self.exiting = False
        self.idle_cond = threading.Condition()
        self.sql_refresh_interval = 3600 # Default: 1 Hour (in seconds).
        self.sql_last_refresh = datetime.datetime.now()
        self.sql_refresh_stmts = ["DO NULL"] # Default: Ask the SQL server for it's current timestamp. (Basically a no-op to keep the connection alive.)
        host = ""
        username = ""
        password = ""
        database = ""

        # Config settings.
        config_file = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), "../config.ini"))
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        # Logger settings
        if logname is not None and \
           type(logname) is str:
            self.logger_name = logname
        else:
            self.logger_name = "GamespyDatabase"
        self.logger = utils.create_logger(config_file, config_sect, self.logger_name)
        self.SQL_LOGLEVEL = self.logger.getEffectiveLevel()

        # Get configuration.
        config = configparser.ConfigParser()
        try:
            config.read(config_file)

            # Get required sections.
            if config.has_section(config_sect) == False:
                raise ValueError("Missing [%s] section in [%s]. Aborting." % (config_sect, config_file))

            if config.has_option(config_sect, "host") == False or \
               config.has_option(config_sect, "username") == False or \
               config.has_option(config_sect, "password") == False or \
               config.has_option(config_sect, "database") == False or \
               config.has_option(config_sect, "dbsalt") == False:
                raise ValueError("[%s] section is missing username/password/host/database/dbsalt options in [%s]. Aborting." % (config_sect, config_file))

            host = config.get(config_sect, "host")
            username = config.get(config_sect, "username")
            password = config.get(config_sect, "password")
            database = config.get(config_sect, "database")
            self.dbsalt = str(config.get(config_sect, "dbsalt"))

            # Get optional sections.
            if config.has_option(config_sect, "logger_sql_loglevel") == True:
                log_level = config.get(config_sect, "logger_sql_loglevel")
                log_lv = logging.getLevelName(log_level)
                if type(log_lv) is not str:
                    self.SQL_LOGLEVEL = log_lv

            if config.has_option(config_sect, "sql_connection_refresh_interval") == True:
                temp_sql_refresh_interval = int(config.get(config_sect, "sql_connection_refresh_interval"))
                if temp_sql_refresh_interval > 0:
                    if temp_sql_refresh_interval < 86399:
                        self.sql_refresh_interval = temp_sql_refresh_interval
                    else:
                        raise ValueError('Invalid value: sql_connection_refresh_interval is greater than 86399 seconds (1 day).')
                else:
                    raise ValueError('Invalid value: sql_connection_refresh_interval is less than 1 second.')

        except Exception as e:
            if self.logger is not None:
                self.logger.log(logging.ERROR, "Could not parse config. Exception raised: [%s]", e)
            sys.exit("Could not parse config.ini. Exception Raised: [%s]" % (e))

        if host == "":
            if self.logger is not None:
                self.logger.log(logging.ERROR, "[%s] section has invalid host option in [%s]. Aborting.", config_sect, config_file)
            sys.exit("Invalid GSDatabase config in config.ini.")

        if username == "":
            if self.logger is not None:
                self.logger.log(logging.ERROR, "[%s] section has invalid username option in [%s]. Aborting.", config_sect, config_file)
            sys.exit("Invalid GSDatabase config in config.ini.")

        if password == "":
            if self.logger is not None:
                self.logger.log(logging.ERROR, "[%s] section has invalid password option in [%s]. Aborting.", config_sect, config_file)
            sys.exit("Invalid GSDatabase config in config.ini.")

        if database == "":
            if self.logger is not None:
                self.logger.log(logging.ERROR, "[%s] section has invalid database option in [%s]. Aborting.", config_sect, config_file)
            sys.exit("Invalid GSDatabase config in config.ini.")

        if self.dbsalt == "":
            if self.logger is not None:
                self.logger.log(logging.ERROR, "[%s] section has invalid dbsalt option in [%s]. Aborting.", config_sect, config_file)
            sys.exit("Invalid GSDatabase config in config.ini.")

        # Open the database.
        self.opendb(host, username, password, database)

        # Clean up.
        config = None
        hostname = None
        username = None
        password = None
        database = None

    def sql_idle_register_stmt(self, stmt):
        if stmt is not None and \
           len(str(stmt)) > 0:
            self.sql_refresh_stmts.append(str(stmt))

    def sql_idle_thread(self):
        self.logger.log(logging.DEBUG, "SQL Idle Thread: Starting.")
        while self.exiting == False:
            next_refresh = self.sql_refresh_interval
            if self.conn != None:
                n = datetime.datetime.now()
                d = n - self.sql_last_refresh
                self.logger.log(logging.DEBUG, "SQL Idle Thread: Old time: [%s], New time [%s], Offset [%s]", str(self.sql_last_refresh), str(n), str(d))
                if d.seconds >= self.sql_refresh_interval or \
                   d.days > 0:
                    self.logger.log(logging.DEBUG, "SQL Idle Thread: Executing connection refresh statements.")
                    if self.sql_refresh_stmts is not None and \
                       type(self.sql_refresh_stmts) is list:
                        for x in self.sql_refresh_stmts:
                            try:
                                with Transaction(self.conn, self.logger) as tx:
                                    tx.nonquery(x)
                            except Error as e:
                                self.logger.log(logging.WARNING, "WARNING: SQL Idle Thread: SQL connection refresh statement failed. error: [%s] statement: [%s]", e, x)
                    else:
                        self.exiting = True
                        self.logger.log(logging.WARNING, "WARNING: SQL Idle Thread: Invalid refresh statement list. Aborting!")
                        break
                else:
                    next_refresh = self.sql_refresh_interval - d.seconds
                self.logger.log(logging.DEBUG, "SQL Idle Thread: Sleeping for %d seconds.", next_refresh)
                self.sql_last_refresh = n
                self.idle_cond.acquire()
                self.idle_cond.wait(next_refresh)
                self.idle_cond.release()
            else:
                self.exiting = True
                break
        if self.conn != None:
            self.conn.close()
            self.conn = None
        self.logger.log(logging.DEBUG, "SQL Idle Thread: Exiting.")

    def opendb(self, host, username, password, database):
        self.conn = None
        try:
            self.conn = mysql.connector.connect(
                host=host,
                user=username,
                passwd=password,
                database=database,
                use_pure=True
            )

            # Create idle / maintenance thread.
            threading.Thread(target=self.sql_idle_thread).start()
        except Error as e:
            self.logger.log(logging.WARNING, "WARNING: Could not connect to database. The error [%s] occured.", e)
        return

    def __del__(self):
        if self.conn != None:
            self.conn.close()
            self.conn = None
        self.close()

    def close(self):
        self.exiting = True
        self.idle_cond.acquire()
        self.idle_cond.notify()
        self.idle_cond.release()

    def initialize_database(self):
        with Transaction(self.conn, self.logger) as tx:
            # I highly doubt having everything in a database be of the type TEXT is a good practice,
            # but I'm not good with databases and I'm not 100% positive that, for instance, that all
            # user id's will be ints, or all passwords will be ints, etc, despite not seeing any
            # evidence yet to say otherwise as far as Nintendo DS games go.

            # Global non-specific tables.
            tx.nonquery("CREATE TABLE IF NOT EXISTS settings (id INT NOT NULL auto_increment, setting_name TEXT, setting_value INT, setting_description TEXT, PRIMARY KEY (id))")
            tx.nonquery("CREATE TABLE IF NOT EXISTS registered_servers (server_addr VARCHAR(16) NOT NULL, server_port VARCHAR(7) NOT NULL, public_addr VARCHAR(16) NOT NULL, " + \
            "server_type INT NOT NULL, server_subtype VARCHAR(100), PRIMARY KEY (server_addr, server_port))")

    def get_dict(self, row):
        if not row:
            return None

        return dict(itertools.izip(row.keys(), row))

    def get_failure(self):
        return str("FAILURE000000000000")

    # NOTE: Check if setting exist and create it if it's not the case
    def checkSetting(self, setting_name, default_setting_value, description):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM settings WHERE setting_name = %s", (setting_name,))
            count = int(row[0])
            if count == 0:
                tx.nonquery("INSERT INTO settings (setting_name, setting_value, setting_description) VALUES (%s,%s,%s)", (setting_name, default_setting_value, description,))

    # Generic server registrations.
    def get_server_type_dictionary(self):
        # Server registration type definitions.
        SERVER_REGISTRATION_TYPES = {"UNDEFINED": 0, "NAS": 1, "NATNEG": 2, "SERVERBROWSER": 3, "QR": 4, "CUSTOM": 5}
        return dict(SERVER_REGISTRATION_TYPES)

    def check_is_registered_server_type(self, server_addr="", server_port="", server_type=0, server_subtype=""):
        count = 0
        ret = False
        if len(str(server_addr)) > 0 and \
           len(str(server_port)) > 0 and \
           server_type > 0:
            with Transaction(self.conn, self.logger) as tx:
                q = "SELECT COUNT(*) FROM registered_servers WHERE server_addr = %s AND server_port = %s AND server_type = %s"
                vals = [server_addr, server_port, server_type]
                if server_type == self.get_server_type_dictionary()["CUSTOM"]:
                    if len(server_subtype) > 0:
                        q += " AND server_subtype = %s"
                        vals.append(server_subtype)
                    else:
                        self.logger.log(logging.DEBUG, "check_is_registered_server_type(): Invalid custom server type defintion. Returning false.")
                        return False
                row = tx.queryall(q, tuple(vals))
                count = int(row[0])
            if count > 0:
                ret = True
        return ret

    def register_server_type(self, server_addr="", server_port="", server_type=0, server_subtype=""):
        if len(str(server_addr)) > 0 and \
           len(str(server_port)) > 0 and \
           server_type > 0:
            q = "INSERT INTO registered_servers (server_addr, server_port, public_addr, server_type"
            vals = [server_addr, server_port, server_addr, server_type]
            a = ""
            if server_type == self.get_server_type_dictionary()["CUSTOM"]:
                if len(server_subtype) > 0:
                    q += ", server_subtype"
                    vals.append(server_subtype)
                    a = ", %s"
                else:
                    self.logger.log(logging.DEBUG, "register_server_type(): Invalid custom server type defintion. Silently refusing to register server.")
                    return
            q += ") VALUES (%s, %s, %s, %s" + a + ") ON DUPLICATE KEY UPDATE server_port = %s"
            vals.append(server_port)
            with Transaction(self.conn, self.logger) as tx:
                tx.nonquery(q, tuple(vals))
        return

    def unregister_server_type(self, server_addr="", server_port="", server_type=0, server_subtype=""):
        if len(str(server_addr)) > 0 and \
           len(str(server_port)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                q = "DELETE FROM registered_servers WHERE server_addr = %s AND server_port = %s"
                vals = [server_addr, server_port]
                if server_type > 0:
                    q += " AND server_type = %s"
                    vals.append(server_type)
                if server_type == self.get_server_type_dictionary()["CUSTOM"]:
                    if len(server_subtype) > 0:
                        q += " AND server_subtype = %s"
                        vals.append(server_subtype)
                    else:
                        self.logger.log(logging.DEBUG, "unregister_server_type(): Invalid custom server type defintion. Silently refusing to unregister server.")
                        return
                tx.nonquery(q, tuple(vals))
        return

    def update_server_type_public_address(self, server_addr="", server_port="", public_addr="", server_type=0, server_subtype=""):
        if len(str(server_addr)) > 0 and \
           len(str(server_port)) > 0 and \
           len(str(public_port)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                q = "UPDATE registered_servers SET public_addr = %s WHERE server_addr = %s AND server_port = %s"
                vals = [public_addr, server_addr, server_port]
                if server_type > 0:
                    q += " AND server_type = %s"
                    vals.append(server_type)
                if server_type == self.get_server_type_dictionary()["CUSTOM"]:
                    if len(server_subtype) > 0:
                        q += " AND server_subtype = %s"
                        vals.append(server_subtype)
                    else:
                        self.logger.log(logging.DEBUG, "update_server_type_public_address(): Invalid custom server type defintion. Silently refusing update.")
                        return
                tx.nonquery(q, tuple(vals))
        return

    def get_server_type_addresses_and_port(self, server_addr="", server_port="", server_type=0, server_subtype=""):
        ret = {}
        if len(str(server_addr)) > 0 and \
           len(str(server_port)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                q = "SELECT * FROM registered_servers WHERE server_addr = %s AND server_port = %s"
                vals = [server_addr, server_port]
                if server_type > 0:
                    q += " AND server_type = %s"
                    vals.append(server_type)
                if len(server_subtype) > 0:
                    q += " AND server_subtype = %s"
                    vals.append(server_subtype)
                rows = tx.queryall(q, tuple(vals))
                for row in rows:
                    ret.append(self.get_dict(row))
        return ret
