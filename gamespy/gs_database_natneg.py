#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import mysql.connector
from mysql.connector import Error
import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils
#import gamespy.gs_database.PortSqliteRow as PortSqliteRow
from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database import GamespyDatabase as GamespyDatabase

class NATNEGGamespyDatabase(GamespyDatabase):
    def __init__(self, logname=None, configsect=None):
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        logger_name = "GamespyDatabase [NATNEG]"
        if logname is not None and \
           type(logname) is str:
            logger_name = logname

        GamespyDatabase.__init__(self, logger_name, config_sect)

    def __del__(self):
        GamespyDatabase.__del__(self)

    def close(self):
        GamespyDatabase.close(self)

    def initialize_database(self):
        with Transaction(self.conn, self.logger) as tx:
            # I highly doubt having everything in a database be of the type TEXT is a good practice,
            # but I'm not good with databases and I'm not 100% positive that, for instance, that all
            # user id's will be ints, or all passwords will be ints, etc, despite not seeing any
            # evidence yet to say otherwise as far as Nintendo DS games go.

            # NATNEG.
            tx.nonquery("CREATE TABLE IF NOT EXISTS natneg_sessions (id INT NOT NULL, gameid VARCHAR(75) NOT NULL, timestamp DATETIME NOT NULL, PRIMARY KEY (id, gameid))")

            tx.nonquery("CREATE TABLE IF NOT EXISTS natneg_clients (natneg_session_id INT NOT NULL, publicaddr VARCHAR(16) NOT NULL, " + \
                        "localaddr VARCHAR(16) NOT NULL, publicport VARCHAR(7) NOT NULL, localport VARCHAR(7), " + \
                        "commport VARCHAR(7), stunaport VARCHAR(7), stunbport VARCHAR(7), generated_client_id INT, hoststate INT, srvcomm_uses_game_port INT, " + \
                        "natneg_server_addr VARCHAR(16) NOT NULL, natneg_server_port VARCHAR(7) NOT NULL, natneg_srv_protocol_ver VARBINARY(1) NOT NULL, " + \
                        "seen_packets INT DEFAULT 0, " + \
                        "PRIMARY KEY (natneg_session_id, publicaddr, publicport), " + \
                        "CONSTRAINT fk_natneg_clients_natneg_session_id FOREIGN KEY (natneg_session_id) REFERENCES natneg_sessions(id) ON UPDATE CASCADE ON DELETE CASCADE, " + \
                        "CONSTRAINT fk_natneg_clients_natneg_server_addr_port FOREIGN KEY (natneg_server_addr, natneg_server_port) REFERENCES " + \
                        "registered_servers(server_addr, server_port) ON UPDATE CASCADE ON DELETE CASCADE)")

            tx.nonquery("CREATE TABLE IF NOT EXISTS natneg_client_status (natneg_session_id INT NOT NULL, natneg_publicaddr VARCHAR(16) NOT NULL, " + \
                        "natneg_publicport VARCHAR(7) NOT NULL, status INT, " + \
                        "PRIMARY KEY(natneg_session_id, natneg_publicaddr, natneg_publicport), " + \
                        "CONSTRAINT fk_natneg_client_status_natneg_session_client_ids FOREIGN KEY (natneg_session_id, natneg_publicaddr, natneg_publicport) " + \
                        "REFERENCES natneg_clients(natneg_session_id, publicaddr, publicport) ON UPDATE CASCADE ON DELETE CASCADE)")

            tx.nonquery("CREATE TABLE IF NOT EXISTS natneg_pre_init_clients (session_id VARBINARY(8) NOT NULL, client_id VARBINARY(8) NOT NULL, state INT, " + \
                        "timestamp DATETIME NOT NULL, client_addr VARCHAR(16) NOT NULL, client_port VARCHAR(7) NOT NULL, client_hoststate VARBINARY(1) NOT NULL, " + \
                        "natneg_server_addr VARCHAR(16) NOT NULL, natneg_server_port VARCHAR(7) NOT NULL, natneg_srv_protocol_ver VARBINARY(1) NOT NULL, " + \
                        "remote_match_id VARBINARY(8) NOT NULL, " + \
                        "PRIMARY KEY(session_id, client_id)," + \
                        "CONSTRAINT fk_natneg_pre_init_clients_natneg_server_addr_port FOREIGN KEY (natneg_server_addr, natneg_server_port) REFERENCES " + \
                        "registered_servers(server_addr, server_port) ON UPDATE CASCADE ON DELETE CASCADE)")

            # Init Default Settings

            # Maximum NATNEG session lifetime in seconds. [(default)=86400 A.K.A 24 Hours]
            self.checkSetting("natneg_session_lifetime", "86400", "Maximum lifetime of a NATNEG session in seconds. Cannot be less than 3600. (1 Hour)")

            # Maximum NATNEG PRE-INIT client lifetime in seconds. [(default)=86400 A.K.A 24 Hours]
            self.checkSetting("natneg_pre-init_lifetime", "86400", "Maximum lifetime of a NATNEG PRE-INIT client in seconds. Cannot be less than 3600. (1 Hour)")

    # NATNEG client constants. (Packet sequence numbers for the INIT packets.)
    NATNEG_CLIENT_PACKET_SEQ_GAME = 0
    NATNEG_CLIENT_PACKET_SEQ_COMM = 1
    NATNEG_CLIENT_PACKET_SEQ_STUNA = 2
    NATNEG_CLIENT_PACKET_SEQ_STUNB = 3
    # NATNEG-related functions
    def check_is_registered_natneg_server(self, server_addr="", server_port=""):
        d = self.get_server_type_dictionary()
        return self.check_is_registered_server_type(server_addr, server_port, d["NATNEG"])

    def register_natneg_server(self, server_addr="", server_port=""):
        self.register_server_type(server_addr, server_port, self.get_server_type_dictionary()["NATNEG"])
        return

    def unregister_natneg_server(self, server_addr="", server_port=""):
        self.unregister_server_type(server_addr, server_port, self.get_server_type_dictionary()["NATNEG"])
        return

    def update_natneg_server_public_address(self, server_addr="", server_port="", public_addr=""):
        self.update_server_type_public_address(server_addr, server_port, public_addr, self.get_server_type_dictionary()["NATNEG"])
        return

    def get_natneg_server_addresses_and_port(self, server_addr="", server_port=""):
        return self.get_server_type_addresses_and_port(server_addr, server_port, self.get_server_type_dictionary()["NATNEG"])

    def purge_natneg_sessions(self):
        self.logger.log(logging.DEBUG, "purge_natneg_sessions(): Deleting ALL!!! NATNEG sessions.")
        with Transaction(self.conn, self.logger) as tx:
            row = tx.nonquery("DELETE FROM natneg_sessions")
        return

    def purge_natneg_expired_sessions(self):
        self.logger.log(logging.DEBUG, "purge_natneg_expired_sessions(): Deleting expired NATNEG sessions.")
        with Transaction(self.conn, self.logger) as tx:
            row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'natneg_session_lifetime'")
            result_ = int(row_[0])
            if result_ < 3600:
                # 24 hours is the default.
                result_ = 86400
            row = tx.nonquery("DELETE FROM natneg_sessions WHERE timestamp <= DATE_SUB(NOW(), INTERVAL %s SECOND)", (result_,))
        return

    def check_natneg_session_exists(self, natneg_session_id=None, gameid=None):
        count = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT COUNT(*) FROM natneg_sessions WHERE id = %s AND gameid = %s", (natneg_session_id, gameid))
                count = int(row[0])

        valid_session = False  # Default, session doesn't exist
        if count > 0:
            valid_session = True  # Valid password
            self.logger.log(logging.DEBUG, "check_natneg_session_exists(): id [%s] for gameid [%s] exists.", str(natneg_session_id), str(gameid))
        else:
            self.logger.log(logging.DEBUG, "check_natneg_session_exists(): id [%s] for gameid [%s] does not exist.", str(natneg_session_id), str(gameid))

        return valid_session

    def create_natneg_session(self, natneg_session_id=None, gameid=None):
        ret = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            if not self.check_natneg_session_exists(natneg_session_id, gameid):
                with Transaction(self.conn, self.logger) as tx:
                    tx.nonquery("INSERT INTO natneg_sessions(id, gameid, timestamp) VALUES(%s, %s, NOW()) ON DUPLICATE KEY UPDATE id = %s", (natneg_session_id, gameid, natneg_session_id))
                    ret = 1
                    self.logger.log(logging.DEBUG, "create_natneg_session(): Created NATNEG session [%s] for gameid [%s].", str(natneg_session_id), str(gameid))
            else:
                ret = 2
        return ret

    def delete_natneg_session(self, natneg_session_id=None, gameid=None):
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            self.logger.log(logging.DEBUG, "delete_natneg_session(): Delete NATNEG session [%s] for gameid [%s].", str(natneg_session_id), str(gameid))
            with Transaction(self.conn, self.logger) as tx:
                row = tx.nonquery("DELETE FROM natneg_sessions WHERE id = %s AND gameid = %s", (natneg_session_id, gameid))
        return

    def get_natneg_session_client_list(self, natneg_session_id=None, gameid=None):
        ret = []
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                rows = tx.queryall("SELECT * FROM natneg_clients " + \
                "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
                "WHERE natneg_clients.natneg_session_id = %s AND natneg_sessions.gameid = %s", (natneg_session_id, gameid))
                for row in rows:
                    ret.append(self.get_dict(row))
        return ret

    def get_natneg_session_server_info(self, natneg_session_id=None, gameid=None):
        ret = []
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                rows = tx.queryall("SELECT publicaddr,publicport,localaddr,localport FROM natneg_clients " + \
                "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
                "WHERE natneg_clients.natneg_session_id = %s AND natneg_sessions.gameid = %s AND natneg_clients.hoststate = 1", (natneg_session_id, gameid))
                for row in rows:
                    ret.append(self.get_dict(row))
        return ret

    def check_natneg_session_client_exists(self, natneg_session_id=None, gameid=None, client_id=None, publicaddr=None, publicport=None):
        count = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            dstr = ""
            q = "SELECT COUNT(*) FROM natneg_clients " + \
            "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
            "WHERE natneg_clients.natneg_session_id = %s " + \
            "AND natneg_sessions.gameid = %s "
            v = [natneg_session_id, gameid]
            if client_id is not None and \
               len(str(client_id)) > 0:
                q += "AND natneg_clients.generated_client_id = %s"
                v.append(str(client_id))
                dstr = "ID: " + str(client_id)
            elif publicaddr is not None and \
                 len(str(publicaddr)) > 0 and \
                 publicport is not None and \
                 len(str(publicport)) > 0:
                q += "AND natneg_clients.publicaddr = %s AND natneg_clients.publicport = %s"
                v.append(str(publicaddr))
                v.append(str(publicport))
                dstr = "IPADDR/PORT: " + str(publicaddr) + ":" + str(publicport)
            if len(v) > 2:
                with Transaction(self.conn, self.logger) as tx:
                    row = tx.queryone(q, tuple(v))
                    count = int(row[0])
        valid_client = False  # Default, session doesn't exist
        if count > 0:
            valid_client = True  # Valid password
            self.logger.log(logging.DEBUG, "check_natneg_session_client_exists(): client [%s] for session [%s] gameid [%s] exists.", \
                            dstr, str(natneg_session_id), str(gameid))
        else:
            self.logger.log(logging.DEBUG, "check_natneg_session_client_exists(): client [%s] for session [%s] gameid [%s] does not exist.", \
                            dstr, str(natneg_session_id), str(gameid))

        return valid_client

    def get_natneg_session_client(self, natneg_session_id=None, gameid=None, client_id=None, publicaddr=None, publicport=None):
        ret = {}
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            q = "SELECT * FROM natneg_clients " + \
            "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
            "WHERE natneg_clients.natneg_session_id = %s " + \
            "AND natneg_sessions.gameid = %s "
            v = [natneg_session_id, gameid]
            if client_id is not None and \
               len(str(client_id)) > 0:
                q += "AND natneg_clients.generated_client_id = %s"
                v.append(str(client_id))
            elif publicaddr is not None and \
                 len(str(publicaddr)) > 0 and \
                 publicport is not None and \
                 len(str(publicport)) > 0:
                q += "AND natneg_clients.publicaddr = %s AND natneg_clients.publicport = %s"
                v.append(str(publicaddr))
                v.append(str(publicport))
            if len(v) > 2:
                with Transaction(self.conn, self.logger) as tx:
                    row = tx.queryone(q, tuple(v))
                    ret = self.get_dict(row)
        return ret

    def get_natneg_session_client_from_ipaddr_client_id(self, session_id=None, client_addr=None, client_port=None, client_id=None):
        ret = {}
        if session_id is not None and \
           len(str(session_id)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0 and \
           client_port is not None and \
           len(str(client_port)) > 0 and \
           client_id is not None and \
           len(str(client_id)) > 0:
            # This function is for fetching the natneg client data without the gameid.
            # (As CONNECT_ACK and REPORT packets don't contain the gameid.)
            #
            # Because some games misbehave, (I.e. They use the NATNEG SRV COMM port
            # when they've instructed the server to use only the game port in their INIT packets.)
            # we need to check multiple ports here.
            q = "SELECT natneg_clients.*, natneg_sessions.gameid FROM natneg_clients " + \
                "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
                "WHERE natneg_clients.generated_client_id = %s AND natneg_clients.natneg_session_id = %s " + \
                "AND natneg_clients.publicaddr = %s AND " + \
                "(natneg_clients.localport = %s OR natneg_clients.localport = '' AND natneg_clients.publicport = %s OR " + \
                "natneg_clients.commport = %s)"
            v = [client_id, session_id, client_addr, client_port, client_port, client_port]
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone(q, tuple(v))
                ret = self.get_dict(row)
        return ret

    def create_natneg_session_client(self, natneg_session_id=None, gameid=None, client_id=None, publicaddr=None, localaddr="", publicport=None, localport="", hoststate=0, server_addr=None, server_port=None, natneg_srv_protocol_ver=None):
        ret = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0 and \
           publicaddr is not None and \
           len(str(publicaddr)) > 0 and \
           publicport is not None and \
           len(str(publicport)) > 0:
            # Only insert data if needed.
            if not self.check_natneg_session_client_exists(natneg_session_id, gameid, None, publicaddr, publicport):
                exq = ""
                q = "INSERT INTO natneg_clients(natneg_session_id, srvcomm_uses_game_port, publicaddr, localaddr, publicport, localport"
                v = [natneg_session_id, 0, publicaddr, localaddr, publicport, localport]

                if client_id is not None and \
                   len(str(client_id)) > 0:
                    q += ", generated_client_id"
                    v.append(client_id)
                    exq += ", %s"
                if server_addr is not None and \
                   len(str(server_addr)) > 0 and \
                   server_port is not None and \
                   len(str(server_port)) > 0:
                    q += ", natneg_server_addr, natneg_server_port"
                    v.append(str(server_addr))
                    v.append(str(server_port))
                    exq += ", %s, %s"
                if natneg_srv_protocol_ver is not None and \
                   len(str(natneg_srv_protocol_ver)) > 0:
                    q += ", natneg_srv_protocol_ver"
                    v.append(natneg_srv_protocol_ver)
                    exq += ", %s"
                if hoststate is not None and \
                   isinstance(hoststate, (int, long)):
                    q += ", hoststate"
                    v.append(hoststate)
                    exq += ", %s"
                with Transaction(self.conn, self.logger) as tx:
                    q += ") VALUES(%s, %s, %s, %s, %s, %s" + exq + ")"
                    tx.nonquery(q, tuple(v))
                    ret = 1
                    self.logger.log(logging.DEBUG, "create_natneg_session_client(): Created NATNEG client [%s] for session [%s] gameid [%s].", \
                                    "ID: " + str(client_id) + " IPADDR/PORT: " + str(publicaddr) + ":" + str(publicport), str(natneg_session_id), str(gameid))
            else:
                ret = 2
        return ret

    def delete_natneg_session_client(self, natneg_session_id=None, gameid=None, client_id=None, publicaddr=None, publicport=None):
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0:
            dstr = ""
            q = "DELETE FROM natneg_clients " + \
            "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
            "WHERE natneg_clients.natneg_session_id = %s AND natneg_sessions.gameid = %s AND "
            v = [natneg_session_id, gameid]
            if client_id is not None and \
            len(str(client_id)) > 0:
                dstr = "ID: " + str(client_id)
                q += "natneg_clients.generated_client_id = %s"
                v.append(str(client_id))
            elif publicaddr is not None and \
                publicport is not None and \
                len(str(publicport)) > 0:
                dstr = "IPADDR/PORT: " + str(publicaddr) + ":" + str(publicport)
                q += "natneg_clients.publicaddr = %s AND natneg_clients.publicport = %s"
                v.append(str(publicaddr))
                v.append(str(publicport))
            else:
                self.logger.log(logging.DEBUG, "delete_natneg_session_client(): Invalid arguments. client_id and publicaddr / publicport are not defined.")
                return
            self.logger.log(logging.DEBUG, "delete_natneg_session_client(): Delete NATNEG client [%s] for session [%s] gameid [%s].", \
                            dstr, str(natneg_session_id), str(gameid))
            if len(v) > 2:
                with Transaction(self.conn, self.logger) as tx:
                    tx.nonquery(q, tuple(v))
        return

    def update_natneg_session_client_publicport(self, natneg_session_id=None, gameid=None, client_addr=None, client_port=None, new_port=None):
        ret = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0 and \
           client_port is not None and \
           len(str(client_port)) > 0 and \
           new_port is not None and \
           len(str(new_port)) > 0 and \
           self.check_natneg_session_client_exists(natneg_session_id, gameid, None, client_addr, client_port) == True:
            # TODO: Lock this to only work if the old port matches another port type (commport, stunaport, stunbport)?
            # That will only work properly if the client entry created by the QR server uses the same publicport as the client's
            # future INIT NATNEG_CLIENT_PACKET_SEQ_GAME packet.
            # For now, just allow updating it regardless.
            q = "UPDATE natneg_clients " + \
            "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
            "SET natneg_clients.publicport = %s " + \
            "WHERE natneg_clients.natneg_session_id = %s AND natneg_sessions.gameid = %s AND " + \
            "natneg_clients.publicaddr = %s AND natneg_clients.publicport = %s"
            v = [new_port, natneg_session_id, gameid, client_addr, client_port]
            with Transaction(self.conn, self.logger) as tx:
                tx.nonquery(q, tuple(v))
                ret = 1
        return ret

    def update_natneg_session_client_data(self, natneg_session_id=None, gameid=None, client_addr=None, client_port=None, \
                                          client_id=None, natneg_srv_protocol_ver=None, \
                                          natneg_server_addr=None, natneg_server_port=None, \
                                          localaddr=None, localport=None, hoststate=None, \
                                          commport=None, stunaport=None, stunbport=None, \
                                          use_game_port=None, seen_packets=None):
        ret = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           gameid is not None and \
           len(str(gameid)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0 and \
           client_port is not None and \
           len(str(client_port)) > 0 and \
           self.check_natneg_session_client_exists(natneg_session_id, gameid, None, client_addr, client_port) == True:
            q = "UPDATE natneg_clients " + \
            "INNER JOIN natneg_sessions ON natneg_clients.natneg_session_id = natneg_sessions.id " + \
            "SET "
            v = []
            if client_id is not None and \
               len(str(client_id)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += "natneg_clients.generated_client_id = %s"
                v.append(client_id)
            if natneg_srv_protocol_ver is not None and \
               len(str(natneg_srv_protocol_ver)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.natneg_srv_protocol_ver = %s"
                v.append(natneg_srv_protocol_ver)
            if natneg_server_addr is not None and \
               len(str(natneg_server_addr)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.natneg_server_addr = %s"
                v.append(str(natneg_server_addr))
            if natneg_server_port is not None and \
               len(str(natneg_server_port)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.natneg_server_port = %s"
                v.append(str(natneg_server_port))
            if localaddr is not None and \
               len(str(localaddr)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.localaddr = %s"
                v.append(str(localaddr))
            if localport is not None and \
               len(str(localport)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.localport = %s"
                v.append(str(localport))
            if commport is not None and \
               len(str(commport)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.commport = %s"
                v.append(str(commport))
            if stunaport is not None and \
               len(str(stunaport)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.stunaport = %s"
                v.append(str(stunaport))
            if stunbport is not None and \
               len(str(stunbport)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.stunbport = %s"
                v.append(str(stunbport))
            if hoststate is not None and \
               len(str(hoststate)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.hoststate = %s"
                v.append(str(hoststate))
            if use_game_port is not None and \
               len(str(use_game_port)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.srvcomm_uses_game_port = %s"
                v.append(str(use_game_port))
            if seen_packets is not None and \
               len(str(seen_packets)) > 0:
                if len(v) >= 1:
                    q += ", "
                q += " natneg_clients.seen_packets = %s"
                v.append(str(seen_packets))
            q += " " + \
            "WHERE natneg_clients.natneg_session_id = %s AND natneg_sessions.gameid = %s AND " + \
            "natneg_clients.publicaddr = %s AND natneg_clients.publicport = %s"
            v.append(natneg_session_id)
            v.append(gameid)
            v.append(client_addr)
            v.append(client_port)
            with Transaction(self.conn, self.logger) as tx:
                tx.nonquery(q, tuple(v))
                ret = 1
        return ret

    def update_natneg_session_client_status(self, natneg_session_id=None, client_id=None, client_addr=None, client_port=None, status=0):
        ret = 0
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0 and \
           client_port is not None and \
           len(str(client_port)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                # We use NATNEG_CLIENT_PACKET_SEQ_GAME (Packet seq 0) for tracking client natneg status.
                tx.nonquery("INSERT INTO natneg_client_status (status, natneg_session_id, natneg_publicaddr, natneg_publicport) " + \
                    "VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE status = %s",
                    (status, natneg_session_id, client_addr, client_port, status))
                ret = 1
        return ret

    def get_natneg_session_client_status(self, natneg_session_id=None, client_id=None, client_addr=None, client_port=None):
        ret = []
        if natneg_session_id is not None and \
           len(str(natneg_session_id)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT status FROM natneg_client_status " + \
                "WHERE natneg_client_status.natneg_session_id = %s AND " + \
                "natneg_client_status.natneg_publicaddr = %s AND natneg_client_status.natneg_publicport = %s",
                (natneg_session_id, client_addr, client_port))
                ret = self.get_dict(row)
        return ret

    def check_natneg_pre_init_client_exists(self, session_id=None, client_id=None):
        count = 0
        if session_id is not None and \
           len(str(session_id)) > 0 and \
           client_id is not None and \
           len(str(client_id)) > 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryall("SELECT COUNT(*) FROM natneg_pre_init_clients " + \
                "WHERE session_id = %s AND client_id = %s",
                (session_id, client_id))
                count = int(row[0])

        valid_session = False  # Default, session doesn't exist
        if count > 0:
            valid_session = True
            self.logger.log(logging.DEBUG, "check_natneg_pre_init_client_exists(): session [%s] client [%s] exists.", \
                            str(session_id), str(client_id))
        else:
            self.logger.log(logging.DEBUG, "check_natneg_pre_init_client_exists(): session [%s] client [%s] does not exist.", \
                            str(session_id), str(client_id))
        return valid_client

    def get_natneg_pre_init_client(self, session_id=None, client_id=None, match_client=True):
        ret = []
        if session_id is not None and \
           len(str(session_id)) > 0 and \
           client_id is not None and \
           len(str(client_id)) > 0:
            self.purge_natneg_expired_pre_init_clients()
            with Transaction(self.conn, self.logger) as tx:
                q = "SELECT * FROM natneg_pre_init_clients " + \
                "WHERE session_id = %s AND client_id "
                if match_client:
                    q = q + "= %s"
                    row = tx.queryone_raw(q, (session_id, client_id))
                    ret = self.get_dict(row)
                else:
                    q = q + "<> %s"
                    rows = tx.queryall_raw(q, (session_id, client_id))
                    for row in rows:
                        ret.append(self.get_dict(row))
        return ret

    def purge_natneg_expired_pre_init_clients(self):
        self.logger.log(logging.DEBUG, "purge_natneg_expired_pre_init_clients(): Deleting expired NATNEG PRE-INIT clients.")
        with Transaction(self.conn, self.logger) as tx:
            row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'natneg_pre-init_lifetime'")
            result_ = int(row_[0])
            if result_ < 3600:
                # 24 hours is the default.
                result_ = 86400
            row = tx.nonquery("DELETE FROM natneg_pre_init_clients WHERE timestamp <= DATE_SUB(NOW(), INTERVAL %s SECOND)", (result_,))
        return

    def delete_natneg_pre_init_client(self, session_id=None, client_id=None):
        ret = 0
        if client_id is not None and \
           len(str(client_id)) > 0 and \
           session_id is not None and \
           len(str(session_id)) > 0:
            self.logger.log(logging.DEBUG, "delete_natneg_pre_init_client(): Delete NATNEG PRE-INIT client [%s] session [%s].", \
                            str(client_id), str(session_id))
            with Transaction(self.conn, self.logger) as tx:
                tx.nonquery("DELETE FROM natneg_pre_init_clients WHERE client_id = %s AND session_id = %s", (client_id, session_id))
            ret = 1
        return ret

    def update_natneg_pre_init_client(self, session_id=None, client_id=None, client_addr=None, client_port=None, client_hoststate=None,
                                      natneg_server_addr=None, natneg_server_port=None, server_proto_ver=1, state=0, remote_match_id=None):
        ret = 0
        if session_id is not None and \
           len(str(session_id)) > 0 and \
           client_id is not None and \
           len(str(client_id)) > 0 and \
           client_addr is not None and \
           len(str(client_addr)) > 0 and \
           client_port is not None and \
           len(str(client_port)) > 0 and \
           client_hoststate is not None and \
           len(str(client_hoststate)) > 0 and \
           natneg_server_addr is not None and \
           len(str(natneg_server_addr)) > 0 and \
           natneg_server_port is not None and \
           len(str(natneg_server_port)) > 0:
            q = "INSERT INTO natneg_pre_init_clients (timestamp, session_id, client_id, state, client_addr, client_port, client_hoststate, " + \
                "natneg_server_addr, natneg_server_port, natneg_srv_protocol_ver, remote_match_id) " + \
                "VALUES(NOW(), %s, %s, %s, %s, %s, %s, %s, %s, %s, '') ON DUPLICATE KEY UPDATE " + \
                "natneg_pre_init_clients.state = %s"
            v = [session_id, client_id, state, client_addr, client_port, client_hoststate,
                 natneg_server_addr, natneg_server_port, server_proto_ver,
                 state]
            if remote_match_id is not None and \
               len(str(remote_match_id)) > 0:
                q += ", natneg_pre_init_clients.remote_match_id = %s"
                v.append(str(remote_match_id))
            with Transaction(self.conn, self.logger) as tx:
                tx.nonquery(q, tuple(v))
                ret = 1
        return ret
