#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils
# import gamespy.gs_database.PortSqliteRow as PortSqliteRow
from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database_nas_auth_check import NASAuthCheckGamespyDatabase as NASAuthCheckGamespyDatabase

class NASGamespyDatabase(NASAuthCheckGamespyDatabase):
    def __init__(self, logname=None, configsect=None):
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        logger_name = "GamespyDatabase [NAS]"
        if logname is not None and \
           type(logname) is str:
            logger_name = logname

        NASAuthCheckGamespyDatabase.__init__(self, logger_name, config_sect)

    def __del__(self):
        NASAuthCheckGamespyDatabase.__del__(self)

    def close(self):
        NASAuthCheckGamespyDatabase.close(self)

    def initialize_database(self):
        with Transaction(self.conn, self.logger) as tx:
            # I highly doubt having everything in a database be of the type TEXT is a good practice,
            # but I'm not good with databases and I'm not 100% positive that, for instance, that all
            # user id's will be ints, or all passwords will be ints, etc, despite not seeing any
            # evidence yet to say otherwise as far as Nintendo DS games go.

            # NASWII tables.
            tx.nonquery("CREATE TABLE IF NOT EXISTS allowed_games (id INT NOT NULL auto_increment, gamecd TEXT, PRIMARY KEY (id))")
            tx.nonquery("CREATE TABLE IF NOT EXISTS consoles (id INT NOT NULL auto_increment, macadr VARCHAR(20) UNIQUE, csnum TEXT, platform TEXT, abuse INT, " + \
                        "enabled INT, PRIMARY KEY (id))")
            tx.nonquery("CREATE TABLE IF NOT EXISTS user (id INT NOT NULL auto_increment, userid TEXT UNIQUE, password TEXT, enabled INT, consoleid INT NOT NULL, "
			"PRIMARY KEY(id), CONSTRAINT fk_user_consoleid FOREIGN KEY (consoleid) REFERENCES consoles(id) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS client_generated_userids (id INT NOT NULL auto_increment, userid INT NOT NULL, clgenuserid TEXT NOT NULL, " + \
                        "PRIMARY KEY (id), CONSTRAINT fk_clgenuserids_users_id FOREIGN KEY (userid) REFERENCES user(id) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS profile (userid INT NOT NULL, profileid INT NOT NULL, enabled INT, gsbrcd VARCHAR(20) NOT NULL, " + \
                        "email TEXT, uniquenick TEXT, pid TEXT, lon TEXT, lat TEXT, loc TEXT, firstname TEXT, lastname TEXT, lastLoginTime DATETIME NOT NULL, preventAutoDelete INT, " + \
                        "stat TEXT, partnerid TEXT, cfc TEXT, bssid TEXT, devname BLOB, birth TEXT, gameid VARCHAR(7) NOT NULL, zipcode TEXT, aim TEXT, PRIMARY KEY(profileid), " + \
                        "CONSTRAINT fk_profile_user_id FOREIGN KEY (userid) REFERENCES user(id) ON UPDATE CASCADE ON DELETE CASCADE, " + \
                        "CONSTRAINT uc_profile UNIQUE (userid, gameid, gsbrcd))")
            tx.nonquery("CREATE TABLE IF NOT EXISTS nas_logins (userid INT NOT NULL UNIQUE, authtoken VARCHAR(85) NOT NULL UNIQUE, data TEXT NOT NULL, loginTime DATETIME NOT NULL, " + \
                        "CONSTRAINT fk_nas_logins_user_id FOREIGN KEY (userid) REFERENCES user(id) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS sessions (session TEXT, profileid INT NOT NULL, loginticket TEXT, createTime DATETIME NOT NULL, " + \
                        "CONSTRAINT fk_sessions_profile_id FOREIGN KEY (profileid) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS buddies (userProfileId INT NOT NULL, buddyProfileId INT NOT NULL, time DATETIME, status INT, notified INT, blocked INT, " + \
                        "CONSTRAINT fk_buddies_user_profile_id FOREIGN KEY (userProfileId) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE, " + \
                        "CONSTRAINT fk_buddies_profile_id FOREIGN KEY (buddyProfileId) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS pending_messages (sourceid INT NOT NULL, targetid INT NOT NULL, msg TEXT, " + \
                        "CONSTRAINT fk_pending_messages_source_id FOREIGN KEY (sourceid) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE, " + \
                        "CONSTRAINT fk_pending_messages_target_id FOREIGN KEY (targetid) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS gamestat_profile (profileid INT NOT NULL, dindex TEXT, ptype TEXT, data TEXT, " + \
                        "CONSTRAINT fk_gamestat_profile_profile_id FOREIGN KEY (profileid) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS gameinfo (profileid INT NOT NULL, dindex TEXT, ptype TEXT, data TEXT, " + \
                        "CONSTRAINT fk_gameinfo_profile_id FOREIGN KEY (profileid) REFERENCES profile(profileid) ON UPDATE CASCADE ON DELETE CASCADE)")
            tx.nonquery("CREATE TABLE IF NOT EXISTS banned (id INT NOT NULL auto_increment, banned_id TEXT, timestamp DATETIME, reason TEXT, ubtime DATETIME, type TEXT, PRIMARY KEY (id))")

            # Create some indexes for performance.
            tx.nonquery("CREATE INDEX IF NOT EXISTS pending_messages_targetid_idx ON pending_messages (targetid)")
            tx.nonquery("CREATE UNIQUE INDEX IF NOT EXISTS sessions_session_idx ON sessions (session)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS sessions_loginticket_idx ON sessions (loginticket)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS sessions_profileid_idx ON sessions (profileid)")
            tx.nonquery("CREATE UNIQUE INDEX IF NOT EXISTS nas_logins_authtoken_idx ON nas_logins (authtoken)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS nas_logins_userid_idx ON nas_logins (userid)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS buddies_userProfileId_idx ON buddies (userProfileId)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS buddies_buddyProfileId_idx ON buddies (buddyProfileId)")
            tx.nonquery("CREATE INDEX IF NOT EXISTS gamestat_profile_profileid_idx ON gamestat_profile (profileid)")

            # Init Default Settings

            # Manual Console Activation [(default)0=Auto Console Activation] [1=Manual Console Activation]
            self.checkSetting("console_manualactivation", "0", "When enabled, makes manual console activation instead of automatically")

            # Allow Banned IP Address [(default)0=Refuse Connection] [1=Allow Connection]
            self.checkSetting("ip_allowbanned", "0", "When enabled, allow banned ip address to connect to the server")

            # Allow Banned MAC Address [(default)0=Refuse Connection] [1=Allow Connection]
            self.checkSetting("mac_allowbanned", "0", "When enabled, allow banned MAC address to connect to the server")

            # Allow Banned Profiles [(default)0=Refuse Connection] [1=Allow Connection]
            self.checkSetting("profile_allowbanned", "0", "When enabled, allow banned profiles to connect to the server")

            # Disable inactive profile purges [(default)0=Purge inactive profiles] [1=Keep inactive profiles]
            self.checkSetting("profile_disablepurge", "0", "When enabled, prevents auto purging inactive profiles.")

            # Allow Banned Users [(default)0=Refuse Connection] [1=Allow Connection]
            self.checkSetting("user_allowbanned", "0", "When enabled, allow banned users to connect to the server")

            # Enable inactive Wii user accounts / game profile purges [(default)0=Keep inactive Wii users / profiles] [1=Purge inactive Wii users / profiles]
            # WARNING: This is disabled by default because the Wii doesn't deal with deleted profiles / user accounts.
            # I.e. The Wii only creates a user account / per-game profile -once-. If it's deleted, the only way to get the Wii to create it again, is to
            # Perform a system initialization (Factory Reset) / Delete the game's save data from Data Management.
            # (Technically you could also change the /shared2/DWC_AUTHDATA file on the Wii's NAND, but that's not user friendly for most people.)
            self.checkSetting("profile_enablewiipurge", "0", "When enabled, causes auto purging of inactive Wii user accounts and game profiles. " + \
            "(DO NOT ENABLE UNLESS YOU KNOW WHAT YOU ARE DOING!)")

    # User functions
    def get_next_available_profileid(self):
        # TODO: Make profile ids start at 1 for each game?

        # TODO: This leads to a race condition if two users try to create accounts at the same time.
        # Instead, it's better to create a new row and return the sqlite ROWID instead.
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT max(profileid) AS m FROM profile")
            r = self.get_dict(row)

        profileid = 1 # Cannot be 0 or else it freezes the game.
        if r != None and r['m'] != None:
            profileid = int(r['m']) + 1

        return profileid

    def create_user(self, clgenuserid, password, macaddr):
        if self.check_client_generated_user_id_exists(clgenuserid) == 0:
            self.logger.log(logging.DEBUG, "create_user(): clgenuserid: [%s] does not exist. Creating new user.", clgenuserid)
            userid = self.get_next_available_userid()
            if len(str(userid)) <= 9:
                password_hash = self.compute_user_password(userid, macaddr, password)
                if password_hash is not None:
                    with Transaction(self.conn, self.logger) as tx:
                        q = "SELECT id FROM consoles WHERE macadr = %s"
                        row = tx.queryone(q, (macaddr,))
                        if len(row) > 0 and int(row[0]) > 0:
                            q = "INSERT INTO user (userid, password, enabled, consoleid) VALUES (%s, %s, %s, %s)"
                            tx.nonquery(q, (str(userid), password_hash, 1, int(row[0])))
                            q = "SELECT id FROM user WHERE userid = %s LIMIT 1"
                            row = tx.queryone(q, (userid,))
                            q = "INSERT INTO client_generated_userids (userid, clgenuserid) VALUES(%s, %s)"
                            tx.nonquery(q, (row[0], clgenuserid))
                        else:
                            self.logger.log(logging.DEBUG, "create_user(): could not get valid console for given macaddr [%s].", macaddr)
                else:
                    self.logger.log(logging.DEBUG, "create_user(): could not generate password hash for userid [%s].", userid)
            else:
                self.logger.log(logging.DEBUG, "create_user(): Out of available userids!")
            return userid

    def internal_insert_profile(self, data):
        profileid = self.get_next_available_profileid()
        self.logger.log(logging.DEBUG, "internal_insert_profile(): New profile [%s] for userid [%s] will be created.", profileid, data["userid"])
        with Transaction(self.conn, self.logger) as tx:
            q = "INSERT INTO profile (profileid, userid, gsbrcd, email, uniquenick, " + \
            "pid, lon, lat, loc, firstname, " + \
            "lastname, stat, partnerid, cfc, bssid, " + \
            "devname, birth, gameid, enabled, zipcode, " + \
            "aim, lastLoginTime) " + \
            "VALUES (%s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,NOW())"
            tx.nonquery(q, (profileid, int(data["uidrow"]), data["gsbrcd"], data["email"], data["uniquenick"], \
            data["pid"], data["lon"], data["lat"], data["loc"], data["firstname"], \
            data["lastname"], data["stat"], data["partnerid"], data["cfc"], data["bssid"], \
            data["devname"], data["birth"], data["gamecd"], data["enabled"], data["zipcode"], \
            data["aim"],))
        return profileid

    # check_for_initial_incomplete_profile(data, profileid):
    # Returns None if given an invalid profileid.
    # Returns 0 if given an incomplete profile and the given gsbrcd / email / uniquenick data args are invalid.
    # Otherwise returns the profileid of a complete profile for the request. (WARNING: The returned id may be
    #           different than the given one, if a complete profile with the given gsbrcd / email / uniquenick
    #           already existed in the db prior to the call.)
    def check_for_initial_incomplete_profile(self, data, profileid):
        profile = self.get_profile_from_profileid(profileid)
        if len(profile) > 0:
            # As some games (Pokemon D/P) do not send a gsbrcd string with their login request for certian WFC services (GTS),
            # This permits those clients to connect if they haven't sent a gsbrcd before.
            #
            # This also attempts to allow those clients to 'reclaim' their profiles should they use a service in the future that
            # their game will send a gsbrcd for.
            #
            # The one flaw with this design is that only one profile without a gsbrcd can exist in the database at a time per user / gameid.
            # If a user attempts to connect multiple games with the same gameid and doesn't use any service that the game will send a gsbrcd for
            # on any of them, they will all get the same profile until one of them does send a gsbrcd. At that point the remaining games will
            # loose 'their' profile id and need to create a new one. Which depending on the game, may require complete re-registration of all of
            # their buddies WFC friend codes and / or loss of in-game content. (Pokemon D/P in this case might cause the player to loose whatever
            # Pokemon and it's held item that was in the GTS at the time.)

            # This get_failure() call is a sentinel value. (mysql cannot handle a composite unique index with a NULL-able value.)
            if str(profile['gsbrcd']) == self.get_failure() and \
               len(str(profile['email'])) <= 0 and \
               len(str(profile['uniquenick'])) <= 0:
                self.logger.log(logging.DEBUG, "check_for_initial_incomplete_profile(): Profile [%s] for userid [%s] is incomplete.", data["profileid"], data["userid"])
                if len(data["gsbrcd"]) > 0 and data["gsbrcd"] != self.get_failure() and \
                   len(data["email"]) > 0 and \
                   len(data["uniquenick"]) > 0:
                    # We have valid gsbrcd data, see if this profile is the correct one to update.
                    q_check = "SELECT profileid FROM profile WHERE userid = %s AND gameid = %s AND gsbrcd = %s"
                    with Transaction(self.conn, self.logger) as tx:
                        row = tx.queryone(q, (int(data["uidrow"]), data["gamecd"], data["gsbrcd"],))
                        if len(row) > 0 and int(row[0]) > 0:
                            # gsbrcd already exists in the db.
                            profileid = int(row[0])
                        else:
                            q_update = "UPDATE profile SET gsbrcd=%s,email=%s,uniquenick=%s WHERE profileid=%s"
                            if str(profile['lon']) == data["lon"] and \
                               str(profile['lat']) == data["lat"] and \
                               str(profile['loc']) == data["loc"] and \
                               str(profile['firstname']) == data["firstname"] and \
                               str(profile['lastname']) == data["lastname"] and \
                               str(profile['stat']) == data["stat"] and \
                               str(profile['partnerid']) == data["partnerid"] and \
                               str(profile['aim']) == data["aim"] and \
                               str(profile['zipcode']) == data["zipcode"] and \
                               str(profile['cfc']) == data["cfc"] and \
                               str(profile['devname']) == data["devname"] and \
                               str(profile['birth']) == data["birth"]:
                               self.logger.log(logging.DEBUG, "check_for_initial_incomplete_profile(): Profile [%s] for userid [%s] will be completed.", \
                                               data["profileid"], data["userid"])
                               tx.nonquery(q_update, (data["gsbrcd"], data["email"], data["uniquenick"], data["profileid"]))
                            else:
                                self.logger.log(logging.DEBUG, "check_for_initial_incomplete_profile(): Attempting to find the correct profile for " + \
                                                "this connection and userid [%s].", userid)
                                q = "SELECT profile.profileid FROM profile INNER JOIN user ON profile.userid = user.id INNER JOIN " + \
                                    "consoles ON user.consoleid = consoles.id WHERE " + \
                                    "profile.lon = %s AND profile.lat = %s AND profile.loc = %s AND profile.firstname = %s AND profile.lastname = %s " + \
                                    "AND profile.stat = %s AND profile.partnerid = %s AND profile.aim = %s AND profile.zipcode = %s AND profile.gameid = %s " + \
                                    "AND profile.cfc = %s AND profile.devname = %s AND profile.birth = %s " + \
                                    "AND user.userid = %s AND consoles.macadr = %s AND profile.gsbrcd = %s AND profile.email = %s AND profile.uniquenick = %s"
                                row = tx.queryone(q, (data["lon"], data["lat"], data["loc"], data["firstname"], data["lastname"], data["stat"], \
                                                  data["partnerid"], data["aim"], data["zipcode"], data["gamecd"], data["cfc"], data["devname"], \
                                                  data["birth"], data["userid"], data["macadr"], self.get_failure(), "", "",))
                                if len(row) > 0 and int(row[0]) > 0:
                                    # Found the correct profile.
                                    profileid = int(row[0])
                                    self.logger.log(logger.DEBUG, "check_for_initial_incomplete_profile(): Profile [%s] for userid [%s] will be completed.", \
                                                    data["profileid"], data["userid"])
                                    tx.nonquery(q_update, (data["gsbrcd"], data["email"], data["uniquenick"], data["profileid"]))
                                else:
                                    # Need to create a new profile.
                                    profileid = self.internal_insert_profile(data)
                else:
                    # Client did not send a gsbrcd with this request.
                    profileid = 0
            # else - No-op. gsbrcd is already defined for this profile.
        else:
            # Invalid profileid.
            profileid = None
        return profileid

    def create_profile(self, data):
        if data["consoleid_row"] != 0:
            profileid = self.check_profile_for_userid_gamecode_gsbrcd_exists(data["userid"], data["gamecd"], data["gsbrcd"])
            if profileid == 0:
                self.logger.log(logging.DEBUG, "create_profile(): A profile for userid: [%s] does not exist. Creating new profile.", data["userid"])

                data["pid"] = "11"  # Always 11??? Is this important? Not to be confused with dwc_pid.
                            # The three games I found it in (Tetris DS, Advance Wars - Days of Ruin, and
                            # Animal Crossing: Wild World) all use \pid\11.
                data["lon"] = "0.000000"  # Always 0.000000?
                data["lat"] = "0.000000"  # Always 0.000000?
                data["enabled"] = 1

                return self.internal_insert_profile(data)
            else:
                self.logger.log(logging.DEBUG, "create_profile(): A profile for userid: [%s] already exists. No need to create a new profile.", data['userid'])
                # Check if we need to update the profile's gdbrcd / email / uniquenick.
                check = self.check_for_initial_incomplete_profile(data, profileid)
                if check is not None and int(check) == 0:
                    # We have an incomplete profile and the client did not send a gsbrcd in the request.
                    return profileid
                else:
                    # We either have an invalid profile, or a (now) complete one.
                    return check
        else:
            self.logger.log(logging.DEBUG, "create_profile(): No console with macaddr [%s] exists. Cannot create profile for userid [%s].", data['macadr'], data['userid'])
        return None

    # Session functions
    def purge_sessions(self):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM sessions")
        return

    # nas server functions
    def register_nas_server(self, server_addr="", server_port=""):
        self.register_server_type(server_addr, server_port, self.get_server_type_dictionary()["NAS"])
        return

    def unregister_nas_server(self, server_addr="", server_port=""):
        self.unregister_server_type(server_addr, server_port, self.get_server_type_dictionary()["NAS"])
        return

    def purge_nas_logins(self):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM nas_logins")
        return

    # (?,?,?,'X',?) X => 1 to a 0 to enable manual console activation
    def console_register(self,postdata):
        if 'csnum' in postdata:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT COUNT(*) FROM consoles WHERE macadr = %s and platform = 'wii'",(postdata['macadr'],))
                result = int(row[0])
                if result == 0:
                    row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'console_manualactivation'")
                    result_ = int(row_[0])
                    if result_ == 0:
                        tx.nonquery("INSERT INTO consoles (macadr, csnum, platform, enabled, abuse) VALUES (%s,%s,'wii','1','0')", (postdata['macadr'], postdata['csnum']))
                    if result_ == 1:
                        tx.nonquery("INSERT INTO consoles (macadr, csnum, platform, enabled, abuse) VALUES (%s,%s,'wii','0','0')", (postdata['macadr'], postdata['csnum']))
            return result > 0
        else:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT COUNT(*) FROM consoles WHERE macadr = %s and platform = 'other'",(postdata['macadr'],))
                result = int(row[0])
                if result == 0:
                    row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'console_manualactivation'")
                    result_ = int(row_[0])
                    if result_ == 0:
                        tx.nonquery("INSERT INTO consoles (macadr, platform, enabled, abuse) VALUES (%s,'other','1','0')", (postdata['macadr'],))
                    if result_ == 1:
                        tx.nonquery("INSERT INTO consoles (macadr, platform, enabled, abuse) VALUES (%s,'other','0','0')", (postdata['macadr'],))
            return result > 0

    def create_initial_user(self,postdata):
        userid = 0
        password = ""
        if 'passwd' in postdata:
            password = postdata['passwd']
        consoleid = self.get_console_id_from_macaddr(postdata['macadr'])
        if consoleid == 0:
            if self.register_console(postdata):
                consoleid = self.get_console_id_from_macaddr(postdata['macadr'])
                if consoleid == 0:
                    self.logger.log(logging.WARNING, "create_initial_user(): console with macaddr [%s] already exists, but we cannot get the row id from the database!", postdata['macadr'])
                else:
                    userid = self.create_user(postdata['userid'], password, postdata['macadr'])
            else:
                self.logger.log(logging.WARNING, "create_initial_user(): could not register console with macaddr [%s]!", postdata['macadr'])
        else:
            userid = self.create_user(postdata['userid'], password, postdata['macadr'])
        return userid

    def create_initial_profile(self,postdata,uid):
        profileid = 0
        console = self.get_console_from_macaddr(postdata['macadr'])
        if console is not None:
            password = ""
            if 'passwd' in postdata:
                password = postdata['passwd']
            uidrow = self.get_user_row_id_from_userid(uid)
            attempt_login = self.check_user_password(uidrow, postdata['macadr'], password)
            if attempt_login is not None and attempt_login == uidrow:
                data = {
                    "userid": uid,
                    "uidrow": uidrow,
                    "consoleid_row": console['id'],
                    "macadr": postdata['macadr'],
                    "gamecd": postdata['gamecd'],
                    "gsbrcd": self.get_failure(),
                    "pid": 0,
                    "enabled": 0,
                    "profileid": 0,
                    "uniquenick": "",
                    "email": "",
                    "devname": "",
                    "birth": "",
                    "cfc": "",
                    "bssid": "",
                    "consoleid": "",
                    "lon": "",
                    "lat": "",
                    "loc": "",
                    "firstname": "",
                    "lastname": "",
                    "stat": "",
                    "partnerid": "",
                    "aim": "",
                    "zipcode": "",
                }
                if 'gsbrcd' in postdata: # Some games don't always send the gsbrcd. (Pokemon D/P when connecting to the GTS.)
                    data["gsbrcd"] = str(postdata['gsbrcd'])
                    data["uniquenick"] = str(utils.base32_encode(int(uid))) + data["gsbrcd"]
                    data["email"] = data["uniquenick"] + "@nds" # The Wii also seems to use @nds.
                if 'bssid' in postdata:
                    data["bssid"] = str(postdata['bssid'])
                if console['platform'] == 'wii':
                    if 'cfc' in postdata:
                        data["cfc"] = str(postdata['cfc'])
                else:
                    if 'devname' in postdata:
                        data["devname"] = str(postdata['devname'])
                    if 'birth' in postdata:
                        data["birth"] = str(postdata['birth'])
                if 'lon' in postdata:
                    data["lon"] = str(postdata['lon'])
                if 'lat' in postdata:
                    data["lat"] = str(postdata['lat'])
                if 'loc' in postdata:
                    data["loc"] = str(postdata['loc'])
                if 'firstname' in postdata:
                    data["firstname"] = str(postdata['firstname'])
                if 'lastname' in postdata:
                    data["lastname"] = str(postdata['lastname'])
                if 'stat' in postdata:
                    data["stat"] = str(postdata['stat'])
                if 'partnerid' in postdata:
                    data["partnerid"] = str(postdata['partnerid'])
                if 'aim' in postdata:
                    data["aim"] = str(postdata['aim'])
                if 'zipcode' in postdata:
                    data["zipcode"] = str(postdata['zipcode'])

                profileid = self.create_profile(data)
        return profileid

    def get_next_available_userid(self):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT max(userid) AS maxuser FROM user")
            r = self.get_dict(row)
        if r == None or r['maxuser'] == None:
            # This cannot be longer than 9 characters, or Pokemon Gen4 games will corrupt their wifi config settings upon parsing the /lm/2 response.
            return '000000002'#Because all zeroes means Dolphin. Don't wanna get confused during debugging later.
        else:
            userid = str(int(r['maxuser']) + 1)
            if len(userid) > 9:
                self.logger.log(logging.WARNING, "userid: [%s] is beyond the maximum limit. You should consider purging old users or set up an additional service.", userid)
                return 99999999999999
            while len(userid) < 9:
                userid = "0"+userid
            return userid

    def generate_authtoken(self, userid, data):
        # Since the auth token passed back to the game will be random, we can make it small enough that there
        # should never be a crash due to the size of the token.
        # ^ real authtoken is 80 + 3 bytes though and I want to figure out what's causing the 52200
        # so I'm matching everything as closely as possible to the real thing
        size = 80
        authtoken = None
        password = ""
        gsbrcd = ""
        gamecode = ""
        uidrow = 0
        if 'gsbrcd' in data:  # Some games don't always send the gsbrcd. (Pokemon D/P when connecting to the GTS.)
            gsbrcd = data['gsbrcd']
        if 'passwd' in data:  # The wii doesn't use passwd.
            password = data['passwd']
        if 'gamecd' in data:
            gamecode = data['gamecd']
        self.logger.log(logging.DEBUG, "generate_authtoken(): Attempting to generate authtoken for userid [%s].", userid)
        uidrow = self.get_user_row_id_from_userid(userid)
        attempt_login = self.perform_login(userid, password, gsbrcd, gamecode) # Returns the profileid if successful.
        if attempt_login is not None and attempt_login > 0 and uidrow > 0:
            # TODO: Another one of those questionable dupe-preventations
            while True:
                with Transaction(self.conn, self.logger) as tx:
                    authtoken = "NDS" + utils.generate_random_str(size)
                    row = tx.queryone("SELECT COUNT(*) FROM nas_logins WHERE authtoken = %s", (authtoken,))
                    count = int(row[0])
                    if count == 0:
                        break

            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT * FROM nas_logins WHERE userid = %s", (uidrow,))
                r = self.get_dict(row)

            if "devname" in data:
                data["devname"] = gs_utils.base64_encode(data["devname"])
            if "ingamesn" in data:
                data["ingamesn"] = gs_utils.base64_encode(data["ingamesn"])

            data = json.dumps(data)

            with Transaction(self.conn, self.logger) as tx:
                if r == None: # no row, add it
                    tx.nonquery("INSERT INTO nas_logins (userid, authtoken, data, loginTime) VALUES (%s, %s, %s, NOW())", (uidrow, authtoken, data,))
                else:
                    tx.nonquery("UPDATE nas_logins SET authtoken = %s, data = %s, loginTime = NOW() WHERE userid = %s", (authtoken, data, uidrow,))
        return authtoken
