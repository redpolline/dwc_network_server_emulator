#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils
# import gamespy.gs_database.PortSqliteRow as PortSqliteRow
from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database_nas_auth_check import NASAuthCheckGamespyDatabase as NASAuthCheckGamespyDatabase

class ProfileGamespyDatabase(NASAuthCheckGamespyDatabase):
    def __init__(self, logname=None, configsect=None):
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        logger_name = "GamespyDatabase [Profile]"
        if logname is not None and \
           type(logname) is str:
            logger_name = logname

        NASAuthCheckGamespyDatabase.__init__(self, logger_name, config_sect)

    def __del__(self):
        NASAuthCheckGamespyDatabase.__del__(self)

    def close(self):
        NASAuthCheckGamespyDatabase.close(self)

    def update_profile(self, profileid, field):
        # Found profile id associated with session key.
        # Start replacing each field one by one.
        # TODO: Optimize this so it's done all in one update.
        # TODO: Check if other values than firstname/lastname are set using this
        # We assume location data is allowed to update (given Pokemon Gen4's allowance
        # of the player to change their location (once) at the globe in the GTS.)
        if field[0] in ["firstname", "lastname", "lat", "lon", "loc"]:
            with Transaction(self.conn, self.logger) as tx:
                q = "UPDATE profile SET {}".format(field[0])
                q = q + " = %s WHERE profileid = %s"
                tx.nonquery(q, (field[1], profileid))

    def save_pending_message(self, sourceid, targetid, msg):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("INSERT INTO pending_messages (sourceid, targetid, msg) VALUES (%s,%s,%s)", (sourceid, targetid, msg,))

    def get_pending_messages(self, profileid):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM pending_messages WHERE targetid = %s", (profileid,))

        messages = []
        for row in rows:
            messages.append(self.get_dict(row))

        return messages

    # Buddy functions
    def add_buddy(self, userProfileId, buddyProfileId):

        # status == 0 -> not authorized
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("INSERT INTO buddies (userProfileId, buddyProfileId, time, status, notified, blocked) " + \
                        "VALUES (%s, %s, NOW(), %s, %s, %s)", (userProfileId, buddyProfileId, 0, 0, 0))

    def auth_buddy(self, userProfileId, buddyProfileId):
        # status == 1 -> authorized
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("UPDATE buddies SET status = %s WHERE userProfileId = %s AND buddyProfileId = %s", (1, userProfileId, buddyProfileId))

    def block_buddy(self, userProfileId, buddyProfileId):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("UPDATE buddies SET blocked = %s WHERE userProfileId = %s AND buddyProfileId = %s", (1, userProfileId, buddyProfileId))

    def unblock_buddy(self, userProfileId, buddyProfileId):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("UPDATE buddies SET blocked = %s WHERE userProfileId = %s AND buddyProfileId = %s", (0, userProfileId, buddyProfileId))

    def get_buddy(self, userProfileId, buddyProfileId):
        profile = {}
        if userProfileId != 0 and buddyProfileId != 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT * FROM buddies WHERE userProfileId = %s AND buddyProfileId = %s", (userProfileId, buddyProfileId))
                profile = self.get_dict(row)
        return profile

    def delete_buddy(self, userProfileId, buddyProfileId):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM buddies WHERE userProfileId = %s AND buddyProfileId = %s", (userProfileId, buddyProfileId))

    def get_buddy_list(self, userProfileId):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM buddies WHERE userProfileId = %s AND blocked = 0", (userProfileId,))

        users = []
        if rows is not None:
            for row in rows:
                users.append(self.get_dict(row))

        return users

    def get_blocked_list(self, userProfileId):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM buddies WHERE userProfileId = %s AND blocked = 1", (userProfileId,))

        users = []
        if rows is not None:
            for row in rows:
                users.append(self.get_dict(row))

        return users

    def get_pending_buddy_requests(self, userProfileId):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM buddies WHERE buddyProfileId = %s AND status = 0", (userProfileId,))

        users = []
        if rows is not None:
            for row in rows:
                users.append(self.get_dict(row))

        return users

    def buddy_need_auth_message(self, userProfileId):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM buddies WHERE buddyProfileId = %s AND status = 1 AND notified = 0", (userProfileId,))

        users = []
        if rows is not None:
            for row in rows:
                users.append(self.get_dict(row))

        return users

    def buddy_sent_auth_message(self, userProfileId, buddyProfileId):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("UPDATE buddies SET notified = %s WHERE userProfileId = %s AND buddyProfileId = %s", (1, userProfileId, buddyProfileId))
