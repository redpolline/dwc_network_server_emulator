#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils
from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database import GamespyDatabase as GamespyDatabase

class NASAuthCheckGamespyDatabase(GamespyDatabase):
    def __init__(self, logname=None, configsect=None):
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        logger_name = "GamespyDatabase [NAS_Auth_Check]"
        if logname is not None and \
           type(logname) is str:
            logger_name = logname

        GamespyDatabase.__init__(self, logger_name, config_sect)

    def __del__(self):
        GamespyDatabase.__del__(self)

    def close(self):
        GamespyDatabase.close(self)

    # User functions
    def purge_expired_profiles(self):
        with Transaction(self.conn, self.logger) as tx:
            row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'profile_disablepurge'")
            result_ = int(row_[0])
            if result_ == 0:
                row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'profile_enablewiipurge'")
                result_ = int(row_[0])
                query = "DELETE profile FROM profile "
                if result_ == 0:
                    query = query + "INNER JOIN user ON profile.userid = user.id INNER JOIN consoles ON user.consoleid = consoles.id " + \
                    "WHERE consoles.platform <> 'wii' AND "
                else:
                    query = query + "WHERE "
                row = tx.nonquery(query + "profile.preventAutoDelete <> 1 AND profile.lastLoginTime <= DATE_SUB(NOW(), INTERVAL 1 YEAR)")
        return

    def check_profile_for_userid_gamecode_gsbrcd_exists(self, userid, gamecode, gsbrcd):
        pid = 0
        # Purge expired profiles.
        self.purge_expired_profiles()
        with Transaction(self.conn, self.logger) as tx:
            q = "SELECT profile.profileid FROM profile INNER JOIN user ON profile.userid = user.id WHERE user.userid = %s"
            if len(str(gsbrcd)) > 0 and str(gsbrcd) != self.get_failure():
                q = q + " and profile.gsbrcd = %s"
                row = tx.queryone(q, (userid, gsbrcd,))
            else:
                q = q + " and profile.gameid = %s"
                row = tx.queryone(q, (userid, gamecode,))
            if len(row) > 0 and int(row[0]) > 0:
                pid = int(row[0])

        valid_profile = 0 # Default, profile doesn't exist.
        if pid > 0:
            valid_profile = pid # Profile exists.
            self.logger.log(logging.DEBUG, "check_profile_for_userid_gamecode_gsbrcd_exists(): Profile for userid [%s] gamecode [%s] gsbrcd [%s] exists.", userid, str(gamecode), str(gsbrcd))
        else:
            self.logger.log(logging.DEBUG, "check_profile_for_userid_gamecode_gsbrcd_exists(): userid [%s] gamecode [%s] gsbrcd [%s] profile does not exist.", userid, str(gamecode), str(gsbrcd))

        return valid_profile

    def check_user_exists_uid(self, userid):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM user WHERE userid = %s", (userid,))
            count = int(row[0])

        valid_user = False  # Default, user doesn't exist
        if count > 0:
            valid_user = True  # Valid password
            self.logger.log(logging.DEBUG, "check_user_exists_uid(): userid [%s] exists.", userid)
        else:
            self.logger.log(logging.DEBUG, "check_user_exists_uid(): userid [%s] does not exist.", userid)

        return valid_user

    def check_user_enabled(self, userid):
        enabled = 0
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT enabled FROM user WHERE userid = %s", (userid,))
            if len(row) > 0 and int(row[0]) > 0:
                enabled = int(row[0])
        return enabled > 0

    def check_profile_enabled(self, userid, gsbrcd):
        enabled = 0
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT enabled FROM profile WHERE userid = %s AND gsbrcd = %s", (userid, gsbrcd,))
            if len(row) > 0 and int(row[0]) > 0:
                enabled = int(row[0])
        return enabled > 0

    def check_client_generated_user_id_exists(self, clgenuserid):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM client_generated_userids WHERE clgenuserid = %s", (clgenuserid,))
            count = int(row[0])

        valid_user = False # Default, user doesn't exist
        if count > 0:
            valid_user = True  # Valid password
            self.logger.log(logging.DEBUG, "check_client_generated_user_id_exists(): clgenuserid [%s] exists.", clgenuserid)
        else:
            self.logger.log(logging.DEBUG, "check_client_generated_user_id_exists(): clgenuserid [%s] does not exist.", clgenuserid)

        return valid_user

    def get_userid_from_user_row_id(self, uidrow):
        userid = 0
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT userid FROM user WHERE id = %s", (uidrow,))
            if len(row) > 0 and int(row[0]) > 0:
                userid = int(row[0])
        return userid

    def get_user_row_id_from_userid(self, userid):
        uidrow = 0
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT id FROM user WHERE userid = %s", (userid,))
            if len(row) > 0 and int(row[0]) > 0:
                uidrow = int(row[0])
        return uidrow

    def get_generated_user_id_from_userid(self, userid):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT client_generated_userids.clgenuserid FROM client_generated_userids INNER JOIN user ON client_generated_userids.userid = user.id WHERE user.userid = %s LIMIT 1", (userid,))
            clgenuserid = 0

        if len(row) > 0 and int(row[0]) > 1:
            clgenuserid = row[0]
        return clgenuserid

    def get_userid_from_generated_user_id(self, clgenuserid):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT user.userid FROM user INNER JOIN client_generated_userids ON user.id = client_generated_userids.userid WHERE client_generated_userids.clgenuserid = %s LIMIT 1", (clgenuserid,))
            userid = 0

        if len(row) > 0 and int(row[0]) > 1:
            userid = row[0]
        return userid

    def check_same_userid_from_generated_user_id(self, clgenuserid, macaddr):
        userid = self.get_userid_from_generated_user_id(clgenuserid)
        same_user = False
        if userid != 0:
            with Transaction(self.conn, self.logger) as tx:
                q = "SELECT consoles.macadr FROM consoles INNER JOIN user ON consoles.userid = user.id WHERE user.userid = %s AND consoles.macadr = %s"
                row = tx.queryone(q, (userid, macaddr,))
                if len(row) > 0 and row == macaddr:
                    same_user = True

        return same_user

    def check_profile_exists(self, profileid):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM profile WHERE profileid = %s", (profileid,))
            count = int(row[0])

        valid_profile = False  # Default, user doesn't exist
        if count > 0:
            valid_profile = True  # Valid password
            self.logger.log(logging.DEBUG, "check_profile_exists(): profileid [%s] exists.", profileid)
        else:
            self.logger.log(logging.DEBUG, "check_profile_exists(): profileid [%s] does not exist.", profileid)

        return valid_profile

    def get_profile_from_profileid(self, profileid):
        profile = {}
        if profileid != 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT * FROM profile WHERE profileid = %s", (profileid,))
                profile = self.get_dict(row)
        return profile

    def perform_login(self, userid, password, gsbrcd, gamecode):
        with Transaction(self.conn, self.logger) as tx:
            q = "SELECT consoles.macadr,consoles.enabled AS cenabled,profile.profileid,profile.enabled AS penabled,user.id,user.enabled AS uenabled " + \
                "FROM profile INNER JOIN user ON profile.userid = user.id INNER JOIN consoles ON" + \
                " user.consoleid = consoles.id " + \
                "WHERE user.userid = %s"
            if len(str(gsbrcd)) > 0 and str(gsbrcd) != self.get_failure():
                q = q + " and profile.gsbrcd = %s"
                row = tx.queryone(q, (userid, gsbrcd,))
            else:
                q = q + " and profile.gameid = %s"
                row = tx.queryone(q, (userid, gamecode,))
            r = self.get_dict(row)
        profileid = None  # Default, user / profile / console doesn't exist
        if r != None:
            uid = self.check_user_password(r['id'], r['macadr'], password)
            if uid is not None and uid == r['id'] and r['cenabled'] == 1 and r['uenabled'] == 1 and r['penabled'] == 1:
                profileid = r['profileid']  # Valid password
                with Transaction(self.conn, self.logger) as tx:
                    tx.nonquery("UPDATE profile SET lastLoginTime = NOW() WHERE profileid = %s", (profileid,))
            else:
                profileid = 0 # Either the user / profile is disabled, or the password is wrong.
        self.logger.log(logging.DEBUG, "perform_login(): userid [%s] gsbrcd [%s] gamecode [%s] profileid [%s] r [%s]", userid, str(gsbrcd), str(gamecode), profileid, str(r))
        return profileid

    def get_console_id_from_macaddr(self, macaddr):
        with Transaction(self.conn, self.logger) as tx:
            q = "SELECT id FROM consoles WHERE macadr = %s LIMIT 1"
            row = tx.queryone(q, (macaddr,))
        id = 0
        if len(row) > 0 and int(row[0]) > 0:
            id = row[0]
        return id

    def get_console_from_macaddr(self, macaddr):
        with Transaction(self.conn, self.logger) as tx:
            q = "SELECT * FROM consoles WHERE macadr = %s LIMIT 1"
            row = tx.queryone(q, (macaddr,))
            if len(row) > 0:
                r = self.get_dict(row)
                if int(r['id']) > 0:
                    return r
        return None

    def get_user_list(self):
        with Transaction(self.conn, self.logger) as tx:
            rows = tx.queryall("SELECT * FROM user")

        users = []
        for row in rows:
            users.append(self.get_dict(row))

        return users

    # Session functions
    # TODO: Cache session keys so we don't have to query the database every time we get a profile id.
    def get_profileid_from_session_key(self, session_key):
        self.delete_expired_sessions()
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT profileid FROM sessions WHERE session = %s", (session_key,))
            r = self.get_dict(row)

        profileid = -1  # Default, invalid session key
        if r != None:
            profileid = r['profileid']

        return profileid

    def get_profileid_from_loginticket(self, loginticket):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT profileid FROM sessions WHERE loginticket = %s", (loginticket,))

        profileid = -1
        if row:
            profileid = int(row[0])

        return profileid

    def get_profile_from_session_key(self, session_key):
        profileid = self.get_profileid_from_session_key(session_key)

        profile = {}
        if profileid != 0:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT profileid FROM sessions WHERE session = %s", (session_key,))
                profile = self.get_dict(row)

        return profile

    def generate_session_key(self, min_size):
        # TODO: There's probably a better way to do this.
        # The point is preventing duplicate session keys.
        while True:
            with Transaction(self.conn, self.logger) as tx:
                session_key = utils.generate_random_number_str(min_size)
                row = tx.queryone("SELECT COUNT(*) FROM sessions WHERE session = %s", (session_key,))
                count = int(row[0])
                if count == 0:
                    return session_key

    def delete_session(self, profileid):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM sessions WHERE profileid = %s", (profileid,))

    def delete_expired_sessions(self):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM sessions WHERE createTime <= DATE_SUB(NOW(), INTERVAL 86400 SECOND)")

    def create_session(self, profileid, loginticket):
        if profileid != None and self.check_profile_exists(profileid) == False:
            return None

        # Remove any old sessions associated with this user id
        self.delete_session(profileid)

        # Create new session
        session_key = self.generate_session_key(8)
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("INSERT INTO sessions (session, profileid, loginticket, createTime) VALUES (%s, %s, %s, NOW())", (session_key, profileid, loginticket,))

        return session_key

    def get_session_list(self, profileid=None):
        sessions = []
        self.delete_expired_sessions()
        with Transaction(self.conn, self.logger) as tx:
            if profileid != None:
                r = tx.queryall("SELECT * FROM sessions WHERE profileid = %s", (profileid,))
            else:
                r = tx.queryall("SELECT * FROM sessions")

        for row in r:
            sessions.append(self.get_dict(row))

        return sessions

    # nas server functions
    def get_nas_login(self, authtoken):
        # get the login data from nas.nintendowifi.net/ac from an authtoken
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM nas_logins WHERE loginTime <= DATE_SUB(NOW(), INTERVAL 86400 SECOND)") # Expire logins after 24 hours.
            row = tx.queryone("SELECT data FROM nas_logins WHERE authtoken = %s", (authtoken,))
            r = self.get_dict(row)
            if r != None:
                self.logger.log(logging.DEBUG, "get_nas_login(): Got nas data: row: [%s] row.keys: [%s] row.values: [%s] row[0]: [%s] r: [%s]", row, row.keys(), row.values(), row[0], r)
                return json.loads(r["data"])
        return None

    def get_nas_login_from_userid(self, userid):
        with Transaction(self.conn, self.logger) as tx:
            tx.nonquery("DELETE FROM nas_logins WHERE loginTime <= DATE_SUB(NOW(), INTERVAL 86400 SECOND)") # Expire logins after 24 hours.
            row = tx.queryone("SELECT data FROM nas_logins INNER JOIN user ON nas_logins.userid = user.id WHERE user.userid = %s", (userid,))
            r = self.get_dict(row)

        if r == None:
            return None
        else:
            return json.loads(r["data"])

    def is_ap_banned(self,postdata):
        if 'bssid' in postdata:
            with Transaction(self.conn, self.logger) as tx:
                row = tx.queryone("SELECT COUNT(*) FROM banned WHERE banned_id = %s AND ubtime > NOW() AND type = 'ap'",(postdata['bssid'],))
                return int(row[0]) > 0

    def is_ip_banned(self,postdata):
        with Transaction(self.conn, self.logger) as tx:
            row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'ip_allowbanned'")
            result_ = int(row_[0])
            if result_ == 0:
                row = tx.queryone("SELECT COUNT(*) FROM banned WHERE banned_id = %s AND ubtime > NOW() AND type = 'ip'",(postdata['ipaddr'],))
                return int(row[0]) > 0
            if result_ == 1:
                return 0

    def is_console_macadr_banned(self,postdata):
        if 'macadr' in postdata:
            with Transaction(self.conn, self.logger) as tx:
                row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'mac_allowbanned'")
                result_ = int(row_[0])
                if result_ == 0:
                    row = tx.queryone("SELECT COUNT(*) FROM banned WHERE banned_id = %s AND ubtime > NOW() and type = 'console'",(postdata['macadr'],))
                    return int(row[0]) > 0
                if result_ == 1:
                    return 0
        else:
            return False

    def pending_console(self,postdata):
        with Transaction(self.conn, self.logger) as tx:
             row = tx.queryone("SELECT COUNT(*) FROM consoles WHERE macadr = %s and enabled = 0",(postdata['macadr'],))
             return int(row[0]) > 0

    def allowed_games(self,postdata):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM allowed_games WHERE gamecd = %s",(postdata['gamecd'][:3],))
            return int(row[0]) > 0

    def is_user_banned(self,uid):
        if uid > 0:
            with Transaction (self.conn, self.logger) as tx:
                row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'user_allowbanned'")
                result_ = int(row_[0])
                if result_ == 0:
                    row = tx.queryone("SELECT COUNT(*) FROM banned WHERE banned_id = %s AND ubtime > NOW() AND type = 'user'",(uid,))
                    return int(row[0]) > 0
                if result_ == 1:
                    return 0
        else:
            return 0

    def is_profile_banned(self,pid):
        with Transaction (self.conn, self.logger) as tx:
            row_ = tx.queryone("SELECT setting_value from settings WHERE setting_name = 'profile_allowbanned'")
            result_ = int(row_[0])
            if result_ == 0:
                row = tx.queryone("SELECT COUNT(*) FROM banned WHERE banned_id = %s AND ubtime > NOW() AND type = 'profile'",(pid,))
                return int(row[0]) > 0
            if result_ == 1:
                return 0
        return 0

    def console_abuse(self,postdata): # ONLY FOR WII CONSOLES
      if 'csnum' in postdata:
         with Transaction(self.conn, self.logger) as tx:
             row = tx.queryone("SELECT COUNT(*) FROM consoles WHERE csnum = %s",(postdata['csnum'],))
             result = int(row[0])
             if result > 2:
                tx.nonquery("UPDATE consoles SET abuse = 1 WHERE csnum = %s", (postdata['csnum'],))
                return True
             else:
                return False

      else:
         return False

    def valid_mac(self,postdata):
        return len(postdata["macadr"]) == 12

    def check_user_password(self, useridrow, macaddr, password):
        with Transaction(self.conn, self.logger) as tx:
            q = "SELECT password " + \
                "FROM user " + \
                "WHERE id = %s"
            row = tx.queryone(q, (useridrow,))
            r = self.get_dict(row)
        result = None  # Default, user doesn't exist
        if r != None:
            attempt_hash = self.compute_user_password(useridrow, macaddr, password)
            if attempt_hash is not None and len(str(attempt_hash)) > 0 and str(r['password']) == str(attempt_hash):
                result = useridrow # Password OK
            else:
                result = 0 # Password Mismatch
        return result

    def compute_user_password(self, userid, macaddr, password):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT platform,csnum FROM consoles WHERE macadr = %s",(macaddr,))
            r = self.get_dict(row)
        tempstr = ""
        compute = None
        if r != None:
            if r['platform'] == "wii" and len(r['csnum']) > 0:
                tempstr = r['csnum'] + macaddr
            else:
                tempstr = "other" + macaddr

            try:
                if password is not None and len(str(password)) > 0:
                    tempstr = tempstr + password
            except NameError, KeyError:
                    # No-op
                    tempstr = tempstr

            # Add salt:
            tempstr = tempstr + self.dbsalt

            # Hash password before entering it into the database.
            # For now I'm using a very simple MD5 hash.
            # TODO: Replace with something stronger later, although it's overkill for the NDS.
            md5 = hashlib.md5()
            md5.update(tempstr)
            compute = md5.hexdigest()
        return compute

    def get_login_profile_via_parsed_authtoken(self, authtoken_parsed):
        console = 0
        userid = self.get_userid_from_generated_user_id(authtoken_parsed['userid'])

        # The Wii does not use passwd, so it may not be filled in here.
        # (The value -if provided- is only part of the real password anyway.)
        password = ""
        if "passwd" in authtoken_parsed:
            password = authtoken_parsed['passwd']

        # Pokemon D/P doesn't send the gsbrcd when connecting to the GTS.
        gsbrcd = ""
        if "gsbrcd" in authtoken_parsed:
            gsbrcd = authtoken_parsed['gsbrcd']
            uniquenick = utils.base32_encode(int(userid)) + gsbrcd

        gamecode = ""
        if "gamecode" in authtoken_parsed:
            gamecode = authtoken_parsed['gamecode']

        profileid = self.check_profile_for_userid_gamecode_gsbrcd_exists(userid, gamecode, gsbrcd)
        if profileid == 0:
            # Profile doesn't exist.
            userid = 0
        else:
            profileid = self.perform_login(userid, password, gsbrcd, gamecode)
            # profileid: 0 == Login failure.
            if profileid > 0:
                if gsbrcd == "" or uniquenick == "":
                    # Attempt to get the settings from the profile. (If they exist.)
                    profile = self.get_profile_from_profileid(profileid)
                    if len(str(profile['gsbrcd'])) > 0:
                        gsbrcd = str(profile['gsbrcd'])
                    if len(str(profile['uniquenick'])) > 0:
                        uniquenick = str(profile['uniquenick'])
        return userid, profileid, gsbrcd, uniquenick
