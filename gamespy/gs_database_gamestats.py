#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import configparser
import hashlib
import itertools
import json
import time
import logging
import threading
import datetime
from contextlib import closing
import collections

import other.utils as utils
import gamespy.gs_utility as gs_utils
# import gamespy.gs_database.PortSqliteRow as PortSqliteRow
from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database_nas_auth_check import NASAuthCheckGamespyDatabase as NASAuthCheckGamespyDatabase

class GameStatsGamespyDatabase(NASAuthCheckGamespyDatabase):
    def __init__(self, logname=None, configsect=None):
        config_sect = "GSDatabase"
        if configsect is not None and \
           type(configsect) is str:
            config_sect = configsect

        logger_name = "GamespyDatabase [GameStats]"
        if logname is not None and \
           type(logname) is str:
            logger_name = logname

        NASAuthCheckGamespyDatabase.__init__(self, logger_name, config_sect)

    def __del__(self):
        NASAuthCheckGamespyDatabase.__del__(self)

    def close(self):
        NASAuthCheckGamespyDatabase.close(self)

    # Gamestats-related functions
    def pd_insert(self, profileid, dindex, ptype, data):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT COUNT(*) FROM gamestat_profile WHERE profileid = %s AND dindex = %s AND ptype = %s", (profileid, dindex, ptype))
            count = int(row[0])
            if count > 0:
                tx.nonquery("UPDATE gamestat_profile SET data = %s WHERE profileid = %s AND dindex = %s AND ptype = %s", (data, profileid, dindex, ptype))
            else:
                tx.nonquery("INSERT INTO gamestat_profile (profileid, dindex, ptype, data) VALUES(%s,%s,%s,%s)", (profileid, dindex, ptype, data))

    def pd_get(self, profileid, dindex, ptype):
        with Transaction(self.conn, self.logger) as tx:
            row = tx.queryone("SELECT * FROM gamestat_profile WHERE profileid = %s AND dindex = %s AND ptype = %s", (profileid, dindex, ptype))
        return self.get_dict(row)
