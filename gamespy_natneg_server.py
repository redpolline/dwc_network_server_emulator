#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Server emulator for *.available.gs.nintendowifi.net and *.master.gs.nintendowifi.net
# Query and Reporting: http://docs.poweredbygamespy.com/wiki/Query_and_Reporting_Overview

import logging
import socket
import struct
import threading
import time
import Queue
from gamespy.gs_database_natneg import NATNEGGamespyDatabase as NATNEGGamespyDatabase
import gamespy.gs_utility as gs_utils
import other.utils as utils
import traceback
import os
import sys
import configparser

from multiprocessing.managers import BaseManager

# Config settings.
config_file = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), "./config.ini"))
config_sect = "GameSpyNatNegServer"

# Logger settings
logger_name = "GameSpyNatNegServer"
logger = utils.create_logger(config_file, config_sect, logger_name)

# Status bit definitions.
STATUS_LOCAL = 0
STATUS_CONNECT2 = 1
STATUS_NN_CONNECT = 2
STATUS_NN_ACK = 3
STATUS_NN_SUCCESS = 4
STATUS_CLIENT = 5
STATUS_SRV_PROTO_V2 = 6

# Server port defintion.
SERVER_TO_SERVER_PORT = 27905

class GameSpyServerDatabase(BaseManager):
    pass

GameSpyServerDatabase.register("get_server_list")
GameSpyServerDatabase.register("modify_server_list")
GameSpyServerDatabase.register("find_servers")
GameSpyServerDatabase.register("find_server_by_address")
GameSpyServerDatabase.register("find_server_by_local_address")
GameSpyServerDatabase.register("add_natneg_server")
GameSpyServerDatabase.register("get_natneg_server")
GameSpyServerDatabase.register("delete_natneg_server")

class GameSpyNatNegServer(object):
    def __init__(self):
        self.session_list = {}
        self.natneg_preinit_session = {}
        self.secret_key_list = gs_utils.generate_secret_keys("gslist.cfg")

        self.server_manager = GameSpyServerDatabase(address=("127.0.0.1", 27500), authkey="")
        self.server_manager.connect()

        # Set up config options.
        config_addr = ""

        # Parse config file.
        global config_sect

        # Get configuration.
        config = configparser.ConfigParser()
        try:
            config.read(config_file)

            # Get optional sections.
            if config.has_section(config_sect) == True:
                if config.has_option(config_sect, "server_address") == True:
                    config_addr = utils.get_ip_from_config(config.get(config_sect, "server_address"))
                    if (config_addr == "0.0.0.0"):
                        raise Exception("Invalid server_address in config!")

            # Set defaults.
            if len(config_addr) == 0:
                config_addr = utils.get_ip_from_config("", False)

        except Exception as e:
            if logger is not None:
                logger.log(logging.ERROR, "Could not parse config section [%s]. Exception raised: [%s]", config_sect, e)
            sys.exit("Could not parse config.ini section [%s]. Exception Raised: [%s]" % (config_sect, e))

        self.address = (config_addr, 27901) # accessible to outside connections (use this if you don't know what you're doing)
        self.server_address = (config_addr, SERVER_TO_SERVER_PORT) # NATNEG Server to NATNEG Server communication.

        self.db = NATNEGGamespyDatabase("GamespyDatabase [%s]" % logger_name)
        self.db.purge_natneg_expired_sessions()


    def __del__(self):
        self.close()

    def close(self):
        if self.socket != None:
            self.socket.close()

        if self.server_socket != None:
            self.server_socket.close()

        if self.db != None:
            if self.address != None and len(self.address[0]) > 0 and len(self.address[1]) > 1:
                self.db.unregister_natneg_server(self.address[0], self.address[1])
            self.db.close()

    def start(self):
        try:
            # Start natneg server
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.bind(self.address)

            self.write_queue = Queue.Queue()

            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.server_socket.bind(self.server_address)

            self.server_write_queue = Queue.Queue()

            logger.log(logging.INFO, "Server is now listening on %s:%s..." % (self.server_address[0], self.server_address[1]))
            threading.Thread(target=self.write_server_queue_worker).start()
            threading.Thread(target=self.server_comm_thread).start()

            logger.log(logging.INFO, "Server is now listening on %s:%s..." % (self.address[0], self.address[1]))
            threading.Thread(target=self.write_queue_worker).start()

            self.db.register_natneg_server(self.address[0], self.address[1])

            while True:
                recv_data, addr = self.socket.recvfrom(2048)

                self.handle_packet(recv_data, addr)
        except:
            logger.log(logging.ERROR, "Unknown exception: %s" % traceback.format_exc())

    def write_queue_send(self, data, address):
        time.sleep(0.05)
        self.socket.sendto(data, address)

    def write_queue_worker(self):
        while True:
            data, address = self.write_queue.get()
            threading.Thread(target=self.write_queue_send, args=(data, address)).start()
            self.write_queue.task_done()

    def write_server_queue_send(self, data, address):
        time.sleep(0.05)
        self.server_socket.sendto(data, address)

    def write_server_queue_worker(self):
        while True:
            data, address = self.server_write_queue.get()
            threading.Thread(target=self.write_server_queue_send, args=(data, address)).start()
            self.server_write_queue.task_done()

    def server_comm_thread(self):
        while True:
            recv_data, addr = self.server_socket.recvfrom(2048)
            self.handle_server_packet(recv_data, addr)

    def handle_server_packet(self, recv_data, addr):
        # Make sure it's a legal packet.
        if self.db.check_is_registered_natneg_server(addr[0], addr[1]) == True:
            logger.log(logging.DEBUG, "Registered server connection from %s:%d...", addr[0], str(addr[1]))

            # Begin output packet.
            output = bytearray([0xfd, 0xfc, 0x1e, 0x66, 0x6a, 0xb2]) # NATNEG Magic.

            # Determine server command.
            if recv_data[0] == "\x01":
                logger.log(logging.DEBUG, "Received NOTIFY NATNEG CLIENT CONNECT from %s:%s...", addr[0], str(addr[1]))

                # Get the client ids from the packet.
                client_a_id = socket.ntohl(recv_data[1])
                client_b_id = socket.ntohl(recv_data[5])

                session_id = socket.ntohl(recv_data[9])

                # Get game id from the packet.
                game_id = utils.get_string(recv_data, 0xD)

                # Get info for client a.
                client_a = self.db.get_natneg_session_client(session_id, game_id, client_a_id)

                # Check and see if the given client a is ours.
                if "natneg_server_addr" in client_a == True and client_a["natneg_server_addr"] == self.address[0] and \
                "natneg_server_port" in client_a == True and client_a["natneg_server_port"] == self.address[1]:

                    # Get info for client b.
                    client_b = self.db.get_natneg_session_client(session_id, game_id, client_b_id)
                    if "publicaddr" in client_b == True and len(client_b["publicaddr"]) > 0:
                        # Generate status flags.
                        status = 1 << STATUS_NN_CONNECT
                        if client_a["publicaddr"] == client_b["publicaddr"]:
                            status = status | (1 << STATUS_LOCAL)
                        else:
                            status = status | (0 << STATUS_LOCAL)

                        # Generate port.
                        a_publicport = ""
                        if "localport" in client_a == True and len(client_a["localport"]) > 0:
                            a_publicport = client_a["localport"]
                        else:
                            if "publicport" in client_a == True and len(client_a["publicport"]) > 0:
                                a_publicport = client_a["publicport"]

                        b_publicport = ""
                        if "localport" in client_b == True and len(client_b["localport"]) > 0:
                            b_publicport = client_b["localport"]
                        else:
                            if "publicport" in client_b == True and len(client_b["publicport"]) > 0:
                                b_publicport = client_b["publicport"]

                        if len(str(a_publicport)) > 0 and len(str(b_publicport)):
                            output += ord(client_a["natneg_srv_protocol_ver"]) # Server Protocol Version
                            output += 0x05 # CONNECT.
                            output += bytearray(client_b["generated_client_id"])
                            output += bytearray([int(x) for x in client_b["publicaddr"].split('.')])
                            output += utils.get_bytes_from_short(int(b_publicport), True)
                            if ord(client_a["natneg_srv_protocol_ver"]) == 0x04:
                                # According to wiki.tockdom.com, RecvdPingFromRemote 0x0 - False 0x1 - True, PingAckdByRemote 0x0 - False 0x1 - True
                                output += bytearray([0x0, 0x0])
                            else:
                                # According to wiki.tockdom.com, this is gotdata, and error code.
                                output += bytearray([0x43, 0x0])

                            # Check which port to send the packet to.
                            write_port = client_a["commport"]
                            if client_a["srvcomm_uses_game_port"] != 0:
                                write_port = a_publicport
                            if int(write_port) > 0:
                                self.write_queue.put((output, (client_a["publicaddr"], int(write_port))))

                                logger.log(logging.DEBUG, "Sent connection request to %s:%s...", client_a["publicaddr"], str(write_port))
                                logger.log(logging.DEBUG, utils.pretty_print_hex(output))

                                # Log that we have sent that data in the database.
                                self.db.update_natneg_session_client_status(session_id, client_b["generated_client_id"], client_b["publicaddr"],
                                                                            client_b["publicport"], status)
                            else:
                                logger.log(logging.DEBUG, "WARNING: Unable to send connection request to client [%s] session [%s] gameid [%s]. " + \
                                           "It's NATNEG SRV COMM port is not set (yet).",
                                           "IP: " + client_a["publicaddr"] + " ID: " + client_a["generated_client_id"], session_id, game_id)
                        else:
                            logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] is incomplete missing port / localport. (SERVFAIL!!!)", client_b_id, session_id, game_id)
                    else:
                        logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] is incomplete missing address. (SERVFAIL!!!)", client_b_id, session_id, game_id)
                else:
                    logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] is not handled by this server. Ignoring request.", client_a_id, session_id, game_id)
            elif recv_data[0] == "\x02":
                logger.log(logging.DEBUG, "Received NOTIFY NATNEG CLIENT PRE-INIT from %s:%s...", addr[0], str(addr[1]))

                # Check packet length.
                if len(recv_data) >= 14:
                    # Get the client ids from the packet.
                    session_id_raw = recv_data[1:5]
                    session_id = utils.get_int(session_id_raw, 0)
                    client_a_id_raw = recv_data[5:9]
                    client_a_id = utils.get_int(client_a_id_raw, 0)
                    client_b_id_raw = recv_data[9:13]
                    client_b_id = utils.get_int(client_b_id_raw, 0)
                    status = socket.ntohl(recv_data[13])

                    # Get info for client.
                    preinit_a = self.db.get_natneg_pre_init_client(session_id_raw, client_a_id_raw)
                    if "session_id" in preinit_a and utils.compare_bytes_in_bytearray(preinit_a["session_id"], session_id_raw) and \
                    "client_id" in preinit_a and utils.compare_bytes_in_bytearray(preinit_a["client_id"], client_a_id_raw) and \
                    "client_addr" in preinit_a and "client_port" in preinit_a and \
                    "state" in preinit_a:
                        # Generate log string for client_b.
                        client_id_log_str = "ID: " + str(preinit_a["client_id"]) + " Session: " + str(session_id) + \
                                            " IP: " + str(preinit_a["client_addr"]) + ":" + int(preinit_a["client_port"])


                        # Check and see if the given client is ours.
                        if "natneg_server_addr" in preinit_a == True and str(preinit_a["natneg_server_addr"]) == self.address[0] and \
                        "natneg_server_port" in preinit_a == True and int(preinit_a["natneg_server_port"]) == self.address[1]:

                            # Fill in the packet data.
                            output += ord(client_a["natneg_srv_protocol_ver"]) # Server Protocol Version
                            output += 0x10 # PREINIT_ACK
                            output += preinit_a["session_id"]
                            output += utils.get_byte_from_bool(int(preinit_a["client_hoststate"]))
                            output += utils.get_byte_from_int(status, False)
                            output += client_b_id_raw

                            self.write_queue.put((output, (str(preinit_a["client_addr"]), int(preinit_a["client_port"]))))

                            self.db.update_natneg_pre_init_client(preinit_a["session_id"], preinit_a["client_id"], preinit_a["client_addr"],
                                                                  preinit_a["client_port"], preinit_a["client_hoststate"],
                                                                  preinit_a["natneg_server_addr"], preinit_a["natneg_server_port"],
                                                                  preinit_a["natneg_srv_protocol_ver"], status, client_b_id_raw)

                            logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK to ...", preinit_a["natneg_srv_protocol_ver"], client_id_log_str)
                            logger.log(logging.DEBUG, utils.pretty_print_hex(output))
                        else:
                            logger.log(logging.DEBUG, "PRE-INIT client [%s] is not handled by this server. Ignoring request.",
                                       client_id_log_str)
                    else:
                        logger.log(logging.DEBUG, "PRE-INIT client [%s] for session [%s] is UNKNOWN. Ignoring request.",
                                   str(client_id), str(session_id))
                else:
                    logger.log(logging.DEBUG, "ERROR: Invalid packet length. Ignoring request from client %s:%s.", addr[0], str(addr[1]))
            else:
                logger.log(logging.DEBUG, "Received UNKNOWN server command [%s] from %s:%s...", ord(recv_data[0]), addr[0], str(addr[1]))
        else:
            logger.log(logging.DEBUG, "UNREGISTERED server connection from %s:%s...", addr[0], str(addr[1]))
        return

    def handle_packet(self, recv_data, addr):
        logger.log(logging.DEBUG, "Connection from %s:%d..." % (addr[0], addr[1]))
        logger.log(logging.DEBUG, utils.pretty_print_hex(recv_data))

        # Make sure it's a legal packet
        if recv_data[0:6] != bytearray([0xfd, 0xfc, 0x1e, 0x66, 0x6a, 0xb2]):
            return

        natneg_srv_protocol_ver = ord(recv_data[6])

        session_id = struct.unpack("<I", recv_data[8:12])[0]
        session_id_raw = recv_data[8:12]

        # Handle commands
        if recv_data[7] == '\x00': # INIT
            logger.log(logging.DEBUG, "Received Ver[%s] initialization from %s:%s..." % (natneg_srv_protocol_ver, addr[0], addr[1]))

#            output = bytearray(recv_data[0:14])
#            output += bytearray([0xff, 0xff, 0x6d, 0x16, 0xb5, 0x7d, 0xea ]) # Checked with Tetris DS, Mario Kart DS, and Metroid Prime Hunters, and this seems to be the standard response to 0x00
#            output[7] = 0x01 # Initialization response
#            self.write_queue.put((output, addr))

            # Decode the init packet.
            gameid = utils.get_string(recv_data, 0x15)
            client_id = "%02x" % ord(recv_data[13])

            # Get port type. (Packet seq # in the mysql database, "porttype" according to GS SDK.)
            # 0x00 - NN_PT_GP (Game Port) (NATNEG_CLIENT_PACKET_SEQ_GAME in gs_database.py.)
            # 0x01 - NN_PT_NN1 (NATNEG Communication Port) (NATNEG_CLIENT_PACKET_SEQ_COMM in gs_database.py.)
            # 0x02 - NN_PT_NN2 (STUN port 0) (NATNEG_CLIENT_PACKET_SEQ_STUNA in gs_database.py.)
            # 0x03 - NN_PT_NN3 (STUN port 1) (NATNEG_CLIENT_PACKET_SEQ_STUNB in gs_database.py.)
            port_type = ord(recv_data[12])

            # Get hoststate. (0x00 - Client, 0x01 - Host)
            hoststate = ord(recv_data[13])

            # Get game port flag. (If set, the game (ab)uses the game port for NATNEG server communication and the second INIT packet is never sent.)
            use_game_port = ord(recv_data[14])

            localip_raw = recv_data[15:19]
            localip_int_le = utils.get_ip(recv_data, 15)
            localip_int_be = utils.get_ip(recv_data, 15, True)
            localip = '.'.join(["%d" % ord(x) for x in localip_raw])
            localport_raw = recv_data[19:21]
            localport = utils.get_short(localport_raw, 0, True)
            localaddr = (localip, localport, localip_int_le, localip_int_be)

            # Create inital port vars.
            rlocalport = None
            rclientport = None
            rcommport = None
            rstunaport = None
            rstunbport = None
            rlocalip = None
            rhoststate = None
            ruse_game_port = None
            rseen_packet = 0

            # Get the requesting client's actual port they want to use.
            if localport > 0:
                rclientport = localport
            else:
                rclientport = addr[1]

            # Generate client id log string.
            connecting_client_id_log_str = "IP:" + str(addr[0]) + ":" + str(addr[1]) + \
                                           " Local Port: " + str(localport) + " ID: " + str(client_id)

            # Figure out what port value to set.
            if port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_GAME:
                rlocalip = localip
                rlocalport = localport
                rhoststate = hoststate
                rseen_packet = (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_GAME)
                # Check for the game (ab)using the game port as the natneg server comm port.
                if int(use_game_port) != 0:
                    rcommport = rclientport
                    ruse_game_port = use_game_port
                logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] sent INIT packet type GAME.",
                           connecting_client_id_log_str, str(session_id), str(gameid))
            elif port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_COMM:
                # Check for the game (ab)using the game port as the natneg server comm port.
                rcommport = rclientport
                rseen_packet = (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_COMM)
                if int(use_game_port) != 0:
                    logger.log(logging.DEBUG, "handle_packet(): Client [%s] session [%s] gameid [%s] is misbehaving. " + \
                               "It has use_game_port set, but sent a NATNEG SRV COMM INIT packet.",
                               connecting_client_id_log_str, str(session_id), str(gameid))
                else:
                    logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] sent INIT packet type NATNEG SRV COMM.",
                               connecting_client_id_log_str, str(session_id), str(gameid))
            elif port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_STUNA:
                rstunaport = rclientport
                rseen_packet = (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_STUNA)
                logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] sent INIT packet type STUNA.",
                           connecting_client_id_log_str, str(session_id), str(gameid))
            elif port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_STUNB:
                rstunbport = rclientport
                rseen_packet = (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_STUNB)
                logger.log(logging.DEBUG, "Client [%s] for session [%s] gameid [%s] sent INIT packet type STUNB.",
                           connecting_client_id_log_str, str(session_id), str(gameid))
            else:
                logger.log(logging.DEBUG, "handle_packet(): WARNING: Client [%s] session [%s] gameid [%s] " + \
                           "sent *unknown* port_type [0x%s] INIT packet.",
                           connecting_client_id_log_str, str(session_id), str(gameid), str(port_type))

            # Create session (if needed) and client.
            session_create = self.db.create_natneg_session(session_id, gameid)
            if session_create > 0:
                updated_client = 0

                # Check and see if the client already exists.
                # Note: The int conversion is due to the DB truncating the value. Resulting in a comparison failure otherwise.
                connecting_client = self.db.get_natneg_session_client(session_id, gameid, client_id)
                if connecting_client is not None and \
                   "generated_client_id" in connecting_client and \
                   int(connecting_client["generated_client_id"]) == int(client_id):

                    # Client exists. Check the public address.
                    if connecting_client["publicaddr"] == addr[0]:
                        # Check publicport.
                        rpublicport = connecting_client["publicport"]

                        # Check the packet type.
                        if port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_GAME:

                            # Check and see if the current public port matches our own.
                            if int(connecting_client["publicport"]) != int(addr[1]):

                                # Client public port is incorrect!
                                # (Probably got an out of order INIT packet seq. Or the game is reusing the session to open another port...)
                                # Need to update it.
                                self.db.update_natneg_session_client_publicport(session_id, gameid,
                                                                                connecting_client["publicaddr"], connecting_client["publicport"], addr[1])

                                # Fix rpublicport.
                                rpublicport = addr[1]

                                logger.log(logging.DEBUG, "Client publicport updated. Was Client [%s] => Now Client [%s] for session [%s] gameid [%s].",
                                           "IP: " + connecting_client["publicaddr"] + ":" + str(connecting_client["publicport"]) + " ID: " + str(connecting_client["generated_client_id"]),
                                           "IP: " + connecting_client["publicaddr"] + ":" + str(addr[1]) + " ID: " + str(connecting_client["generated_client_id"]),
                                           session_id, gameid)

                            # Check and see if the NATNEG SRV COMM port is defined. (If so, don't overwrite it with the game port.)
                            if len(str(connecting_client["commport"])) > 0 and int(connecting_client["commport"]) > 0:
                                rcommport = None
                                if int(connecting_client["srvcomm_uses_game_port"]) != 0 and int(connecting_client["commport"]) != int(rclientport):
                                    logger.log(logging.DEBUG, "Client [%s] session [%s] gameid [%s] is *really* misbehaving. It has use_game_port set, and " + \
                                               "has defined different ports for game [%s] and server [%s] comms.",
                                               "IP: " + connecting_client["publicaddr"] + " ID: " + str(connecting_client["generated_client_id"]),
                                               str(session_id), str(gameid), str(rclientport), str(connecting_client["commport"]))

                        if port_type != self.db.NATNEG_CLIENT_PACKET_SEQ_GAME:
                            logger.log(logging.DEBUG, "EXTRA DEBUG::::::::::: UPDATE DB COMM [%s] STUNA [%s] STUNB [%s]", str(rcommport), str(rstunaport), str(rstunbport))

                        # Update the seen packet flag.
                        rseen_packet = rseen_packet | int(connecting_client["seen_packets"])

                        # Update the client data.
                        updated_client = self.db.update_natneg_session_client_data(session_id, gameid, addr[0], rpublicport,
                                                                                   client_id, natneg_srv_protocol_ver,
                                                                                   self.address[0], self.address[1],
                                                                                   rlocalip, rlocalport, rhoststate,
                                                                                   rcommport, rstunaport, rstunbport,
                                                                                   ruse_game_port, rseen_packet)
                        # Clean up rpublicport.
                        del rpublicport
                    else:
                        logger.log(logging.DEBUG,
                                   "WARNING POSSIBLE MALICIOUS CLIENT: Connecting client [%s] for session [%s] gameid [%s] does not match client public address in database!",
                                   "IP: " + addr[0] + "ID: " + str(client_id), session_id, gameid)
                else:
                    # Client doesn't exist.
#			Natneg client DB rules:
#			- It's impossible for the publicport to be undefined due to the DB constraints.

#                        - If the publicport is identical to another port, then the client id is defined.
#			(Due to the other port having been defined by an out-of-order INIT packet.)

                    # Check packet seq type.
                    if port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_GAME:
                        # We can define the localport. Calling create session client is ok.
                        client_create = self.db.create_natneg_session_client(session_id, gameid, client_id, addr[0], localip,
                                                                             addr[1], localport, hoststate, self.address[0],
                                                                             self.address[1], natneg_srv_protocol_ver)
                    else:
                        # This is bad. The client doesn't exist and we've gotten an out of order INIT packet sequence.
                        # Solution: This packet defines the client id, so create the client in the db and set it's port to the current one.
                        # (When we get the game packet, we'll update the port number in the DB.)
                        client_create = self.db.create_natneg_session_client(session_id, gameid, client_id, addr[0], localip,
                                                                             addr[1], 0, hoststate, self.address[0],
                                                                             self.address[1], natneg_srv_protocol_ver)

                    if client_create > 0:
                        # Check port type.
                        if port_type != self.db.NATNEG_CLIENT_PACKET_SEQ_GAME:
                            logger.log(logging.DEBUG, "EXTRA DEBUG::::::::::: UPDATE DB COMM [%s] STUNA [%s] STUNB [%s]", str(rcommport), str(rstunaport), str(rstunbport))

                        # Update the client data.
                        updated_client = self.db.update_natneg_session_client_data(session_id, gameid, addr[0], addr[1],
                                                                                   client_id, natneg_srv_protocol_ver,
                                                                                   self.address[0], self.address[1],
                                                                                   rlocalip, rlocalport, rhoststate,
                                                                                   rcommport, rstunaport, rstunbport,
                                                                                   ruse_game_port, rseen_packet)

                        logger.log(logging.DEBUG, "Created client [%s] for session [%s] gameid [%s]",
                                   connecting_client_id_log_str, str(session_id), str(gameid))
                    else:
                        # Couldn't create client in db.
                        logger.log(logging.DEBUG, "handle_packet(): Could not create a NATNEG client with id: [%s] for " + \
                                   "session [%s] and gameid: [%s]",
                                   connecting_client_id_log_str, str(session_id), str(gameid))

                # Clean up the connecting_client var. (We've updated it, so it's no longer valid.)
                del connecting_client

                # Check for updated client.
                if updated_client > 0:

                    # Send INIT_ACK.
                    output = bytearray(recv_data[0:14])
                    output += bytearray([0xFF, 0xFF]) # Unknown. (Always 0xFF.)
                    output[7] = 0x01 # INIT_ACK.
                    self.write_queue.put((output, (addr[0], addr[1])))
                    logger.log(logging.DEBUG, "handle_packet(): Sent INIT_ACK to %s:%s...", addr[0], str(addr[1]))
                    logger.log(logging.DEBUG, utils.pretty_print_hex(output))

                    # Only send CONNECT packets if this is a game (port type 0x0) or comm (port type 0x1) packet.
                    if port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_GAME or \
                       port_type == self.db.NATNEG_CLIENT_PACKET_SEQ_COMM:

                        # Check and see if we have enough data about the client to send CONNECT packets.
                        connecting_client = self.db.get_natneg_session_client(session_id, gameid, client_id)
                        if len(connecting_client) > 0 and \
                           "publicaddr" in connecting_client and \
                           len(connecting_client["publicaddr"]) > 0 and \
                           "commport" in connecting_client and \
                           len(str(connecting_client["commport"])) > 0 and \
                           "srvcomm_uses_game_port" in connecting_client and \
                           len(str(connecting_client["srvcomm_uses_game_port"])) > 0 and \
                           int(connecting_client["commport"]) > 0 or \
                           int(connecting_client["srvcomm_uses_game_port"]) > 0 and \
                           "localport" in connecting_client and \
                           "publicport" in connecting_client and \
                           len(str(connecting_client["localport"])) > 0 and \
                           len(str(connecting_client["publicport"])) > 0 and \
                           int(connecting_client["localport"]) > 0 or \
                           int(connecting_client["publicport"]) > 0 and \
                           (int(connecting_client["seen_packets"]) & (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_GAME)) != 0 and \
                           (int(connecting_client["seen_packets"]) & (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_COMM)) != 0:

                            # Figure out the connecting client's game port.
                            connecting_client_publicport = connecting_client["publicport"]
                            if int(connecting_client["localport"]) > 0:
                                connecting_client_publicport = connecting_client["localport"]

                            logger.log(logging.DEBUG, "EXTRA DEBUG::::::: ccl localport [%s], ccl publicport [%s], ccp [%s]",
                                       connecting_client["localport"], connecting_client["publicport"], connecting_client_publicport)

                            # Get the client list for this NATNEG session.
                            client_list = self.db.get_natneg_session_client_list(session_id, gameid)
                            if len(client_list) > 1:

                                # Iterate through the client list and notify each existing client of the new one.
                                for client in client_list:
                                    if "generated_client_id" not in client or client["generated_client_id"] == client_id:
                                        logger.log(logging.DEBUG, "Not sending CONNECT packet to client for session [%s] gameid [%s]. client ID not set yet.",
                                                   session_id, gameid)
                                        continue

                                    # 1) Get the current client's port. (localaddr's port if defined else serveraddr's if defined else addr's port.)
                                    publicport = client["publicport"]
                                    if len(str(client["localport"])) > 0 and int(client["localport"]) > 0:
                                        publicport = client["localport"]

                                    # If publicport is not set, then we cannot send info about this client to the connecting client yet. (There's no game port to connect to.)
                                    if len(str(publicport)) == 0 or int(publicport) <= 0:
                                        logger.log(logging.DEBUG, "Not sending CONNECT packet for client [%s] session [%s] gameid [%s] as it's game port isn't set yet.",
                                                   "ID:" + str(client["generated_client_id"]) + " IP:" + str(client["publicaddr"]), str(session_id), str(gameid))
                                        continue

                                    # If the commport is not set or the game isn't using the game port as the commport, then
                                    # then we cannot send info about the connecting client to this client yet. (There's no commport for us to notify.)
                                    if len(str(client["commport"])) <= 0 or int(client["commport"]) <= 0:
                                        logger.log(logging.DEBUG, "Not sending CONNECT packet to client [%s] session [%s] gameid [%s] as it's NATNEG SRV COMM port isn't set yet.",
                                                   "ID:" + str(client["generated_client_id"]) + " IP:" + str(client["publicaddr"]), str(session_id), str(gameid))
                                        continue

                                    if (int(client["seen_packets"]) & (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_GAME)) == 0 or \
                                       (int(client["seen_packets"]) & (1 << self.db.NATNEG_CLIENT_PACKET_SEQ_COMM)) == 0:
                                        logger.log(logging.DEBUG, "Not sending CONNECT packets for client [%s] session [%s] gameid [%s] " + \
                                                   "as the clients haven't sent enough GAME / NATNEG SRV COMM packets yet.",
                                                   "ID:" + str(client["generated_client_id"]) + " IP:" + str(client["publicaddr"]), str(session_id), str(gameid))
                                        continue

                                    # Wait 10 milliseconds. (Protocol requirement.)
                                    time.sleep(0.01)

                                    # Generate status flags.
                                    status = 1 << STATUS_NN_CONNECT
                                    if client["publicaddr"] == addr:
                                        status = status | (1 << STATUS_LOCAL)
                                    else:
                                        status = status | (0 << STATUS_LOCAL)
                                    if natneg_srv_protocol_ver < 3:
                                        status = status | (1 << STATUS_SRV_PROTO_V2)
                                    else:
                                        status = status | (0 << STATUS_SRV_PROTO_V2)

                                    # 2) Send to requesting client
                                    output = bytearray(recv_data[0:12])
                                    output += bytearray([int(x) for x in client["publicaddr"].split('.')])
                                    output += utils.get_bytes_from_short(int(publicport), True)

                                    if natneg_srv_protocol_ver == 0x04:
                                        # According to wiki.tockdom.com, RecvdPingFromRemote 0x0 - False 0x1 - True, PingAckdByRemote 0x0 - False 0x1 - True
                                        output += bytearray([0x0, 0x0])
                                    else:
                                        # According to wiki.tockdom.com, this is gotdata, and error code.
                                        output += bytearray([0x42, 0x00])

                                    output[7] = 0x05 # CONNECT

                                    # Check which port to send the packet to.
                                    write_port = connecting_client["commport"]
                                    if connecting_client["srvcomm_uses_game_port"] != 0:
                                        write_port = connecting_client_publicport

                                    # Send the packet.
                                    self.write_queue.put((output, (connecting_client["publicaddr"], int(write_port))))

                                    logger.log(logging.DEBUG, "Sent connection request to %s:%s...", connecting_client["publicaddr"], write_port)
                                    logger.log(logging.DEBUG, utils.pretty_print_hex(output))

                                    # 3) Log that we have sent that data in the database.
                                    self.db.update_natneg_session_client_status(session_id, client_id, connecting_client["publicaddr"],
                                                                                connecting_client["publicport"], status)

                                    # 4) Send to other client
                                    if client["natneg_server_addr"] == self.address[0] and \
                                       int(client["natneg_server_port"]) == int(self.address[1]):
                                        output = bytearray(recv_data[0:12])
                                        output += bytearray([int(x) for x in connecting_client["publicaddr"].split('.')])
                                        output += utils.get_bytes_from_short(int(connecting_client_publicport), True)

                                        if ord(client["natneg_srv_protocol_ver"]) == 0x04:
                                            # According to wiki.tockdom.com, RecvdPingFromRemote 0x0 - False 0x1 - True, PingAckdByRemote 0x0 - False 0x1 - True
                                            output += bytearray([0x0, 0x0])
                                        else:
                                            # According to wiki.tockdom.com, this is gotdata, and error code.
                                            output += bytearray([0x42, 0x00])

                                        output[7] = 0x05 # CONNECT

                                        # Check which port to send the packet to.
                                        write_port = client["commport"]
                                        if client["srvcomm_uses_game_port"] != 0:
                                            write_port = publicport
                                        self.write_queue.put((output, (client["publicaddr"], int(write_port))))

                                        logger.log(logging.DEBUG, "EXTRA DEBUG:::::::: client_publicport [%s], client_commport [%s], write_port [%s]",
                                                   publicport, client["commport"], write_port)

                                        logger.log(logging.DEBUG, "Sent connection request to %s:%s...", client["publicaddr"], write_port)
                                        logger.log(logging.DEBUG, utils.pretty_print_hex(output))

                                        # 5) Log that we have sent that data in the database.
                                        self.db.update_natneg_session_client_status(session_id, client["generated_client_id"],
                                                                                    client["publicaddr"], client["publicport"], status)
                                    else:
                                        # Tell the other server to send the data to the client.


                                        # WARNING: TODO: If the client doesn't respond to the connecting client in time, the connection will fail.
                                        # We should send the notification packet to the remote server first to allow it enough time to respond and
                                        # and sync up with us.

                                        output = bytearray([0x1])
                                        output += bytearray(socket.htonl(client["generated_client_id"]))
                                        output += bytearray(socket.htonl(int(client_id)))
                                        output += bytearray(socket.htonl(int(session_id)))
                                        output += bytearray(gameid)
                                        output += bytearray([0x0])
                                        self.server_write_queue.put((output, (client["natneg_server_addr"], SERVER_TO_SERVER_PORT)))
                                        logger.log(logging.DEBUG, "Sent server request to %s:%s...", client["natneg_server_addr"], str(SERVER_TO_SERVER_PORT))

                                # Clean up the client list.
                                del client_list
                            else:
                                logger.log(logging.DEBUG, "Cannot send CONNECT notifications yet. Waiting on additional clients for session [%s] gameid [%s].", session_id, gameid)
                        else:
                            # Nothing to do here. We don't have enough data to send CONNECT packets back to the connecting client.
                            logger.log(logging.DEBUG, "Cannot send CONNECT notifications yet. Client [%s] for session [%s] gameid [%s] hasn't sent all of it's INIT packets.",
                                       connecting_client_id_log_str, str(session_id), str(gameid))
                    else:
                            # Nothing to do here. Current packet is either STUNA or STUNB. Which do not trigger sending CONNECT packets.
                            if port_type == 2:
                                dstr = "STUNA"
                            elif port_type == 3:
                                dstr = "STUNB"
                            else:
                                dstr = "UNKNOWN 0x" + str(port_type)
                            logger.log(logging.DEBUG, "Will not send CONNECT packets as the connecting client [%s] session [%s] gameid [%s]'s port type is [%s].",
                                       connecting_client_id_log_str, str(session_id), str(gameid), dstr)
                            del dstr
                else:
                    logger.log(logging.DEBUG, "handle_packet(): Could not update a NATNEG client with id: [%s] for session [%s] and gameid: [%s]",
                               str(client_id), str(session_id), str(gameid))
            else:
                logger.log(logging.DEBUG, "handle_packet(): Could not create a NATNEG session with id: [%s] and gameid: [%s]", str(session_id), str(gameid))

        elif recv_data[7] == '\x06': # CONNECT_ACK
            # Metroid Prime Hunters (Server Protocol Version 2) doesn't seem to send anything else after this.
            client_id = "%02x" % ord(recv_data[13])
            logger.log(logging.DEBUG, "Received Ver[%s] CONNECT_ACK command from [%s:%s].", natneg_srv_protocol_ver, addr[0], addr[1])

            client = self.db.get_natneg_session_client_from_ipaddr_client_id(session_id, addr[0], addr[1], client_id)

            if client is not None and \
               "publicaddr" in client and client["publicaddr"] == addr[0] and \
               "localport" in client and "publicport" in client and \
               "natneg_session_id" in client and session_id == client["natneg_session_id"] and \
               "generated_client_id" in client and len(str(client["generated_client_id"])) > 0 and \
               "gameid" in client and len(str(client["gameid"])) > 0 and \
               len(str(client["localport"])) > 0 or len(str(client["publicport"])) > 0:
                publicport = client["publicport"]
                if len(str(client["localport"])) > 0 and int(client["localport"]) > 0:
                    publicport = client["localport"]

                # Get previous status and update it.
                s = self.db.get_natneg_session_client_status(session_id, client["generated_client_id"], client["publicaddr"], publicport)
                if "status" in s:
                    status = s["status"] | (1 << STATUS_NN_ACK)
                    self.db.update_natneg_session_client_status(session_id, client["generated_client_id"], client["publicaddr"], publicport, status)
                else:
                    logger.log(logging.DEBUG, "ERROR: Unknown / missing NATNEG status for sessionid [%s] clientid [%s] gameid [%s] " + \
                               "at client address [%s:%s].", str(session_id), str(client_id), client["gameid"], client["publicaddr"], str(publicport))
            else:
                logger.log(logging.DEBUG, "Client at [%s:%s] for remote client id [%s] with session [%s] is not recognised. Ignoring.",
                           addr[0], str(addr[1]), str(client_id), str(session_id))

        elif recv_data[7] == '\x0a': # Address check. Note: UNTESTED!
            client_id = "%02x" % ord(recv_data[13])
            logger.log(logging.DEBUG, "Received Ver[%s] address check command from %s:%s...", natneg_srv_protocol_ver, addr[0], str(addr[1]))

            output = bytearray(recv_data[0:15])
            output += bytearray([int(x) for x in addr[0].split('.')])
            output += utils.get_bytes_from_short(addr[1], True)
            output += bytearray(recv_data[len(output):])

            output[7] = 0x0b
            self.write_queue.put((output, addr))

            logger.log(logging.DEBUG, "Sent address check response to %s:%d..." % (addr[0], addr[1]))
            logger.log(logging.DEBUG, utils.pretty_print_hex(output))

        elif recv_data[7] == '\x0c': # Natify
            port_type = "%02x" % ord(recv_data[12])
            logger.log(logging.DEBUG, "Received Ver[%s] natify command from %s:%s...", natneg_srv_protocol_ver, addr[0], str(addr[1]))

            output = bytearray(recv_data)
            output[7] = 0x02 # ERT Test
            self.write_queue.put((output, addr))

            logger.log(logging.DEBUG, "Sent natify response to %s:%d..." % (addr[0], addr[1]))
            logger.log(logging.DEBUG, utils.pretty_print_hex(output))

        elif recv_data[7] == '\x0d': # Report
            client_id = "%02x" % ord(recv_data[13])
            result = ord(recv_data[14]) # natneg_result

            logger.log(logging.DEBUG,
                       "Received Ver[%s] report command from [%s:%s] using sessionid [%s] and clientid [%s]. Natneg result [%s].",
                       str(natneg_srv_protocol_ver), addr[0], str(addr[1]), str(session_id), str(client_id), str(result))

            client = self.db.get_natneg_session_client_from_ipaddr_client_id(session_id, addr[0], addr[1], client_id)
            if client is not None and \
               "publicaddr" in client and client["publicaddr"] == addr[0] and \
               "localport" in client and "publicport" in client and \
               "natneg_session_id" in client and session_id == client["natneg_session_id"] and \
               "generated_client_id" in client and len(str(client["generated_client_id"])) > 0 and \
               "gameid" in client and len(str(client["gameid"])) > 0 and \
               len(str(client["localport"])) > 0 or len(str(client["publicport"])) > 0:

                publicport = client["publicport"]
                if len(str(client["localport"])) > 0 and int(client["localport"]) > 0:
                    publicport = client["localport"]

                # Get previous status and update it.
                s = self.db.get_natneg_session_client_status(session_id, client["generated_client_id"], client["publicaddr"], publicport)
                if "status" in s:
                    if result == 1:
                        status = s["status"] | (1 << STATUS_NN_SUCCESS)
                    else:
                        status = s["status"] | (0 << STATUS_NN_CONNECT) | (0 << STATUS_NN_ACK)
                    self.db.update_natneg_session_client_status(session_id, client["generated_client_id"], client["publicaddr"], publicport, status)
                else:
                    logger.log(logging.DEBUG, "ERROR: Unknown / missing NATNEG status for sessionid [%s] clientid [%s] gameid [%s] " + \
                               "at client address [%s:%s].", str(session_id), str(client_id), client["gameid"], client["publicaddr"], str(publicport))
            else:
                logger.log(logging.DEBUG, "Client at [%s]:[%s] for remote client id [%s] with session [%s] is not recognised. Ignoring.",
                           addr[0], addr[1], str(client_id), str(session_id))

            # Report response
            output = bytearray(recv_data[:21])
            output[7] = 0x0e # Report response
            output[14] = 0 # Clear byte to match real server's response
            self.write_queue.put((output, addr))
            logger.log(logging.DEBUG, utils.pretty_print_hex(output))

        elif recv_data[7] == '\x0f': # PRE-INIT
            # Natneg v4 command thanks to Pipian.
            # Only seems to be used in very few DS games (namely, Pokemon Black/White/Black 2/White 2).
            logger.log(logging.DEBUG, "Received Ver[%s] PRE-INIT command from %s:%s.", natneg_srv_protocol_ver, addr[0], addr[1])
            logger.log(logging.DEBUG, utils.pretty_print_hex(recv_data))

            client_hoststate = ord(recv_data[12])
            status = ord(recv_data[13])
            client_a_id = utils.get_int(recv_data[14:18], 0)
            client_a_id_raw = recv_data[14:18]

            # Generate ID log strings.
            client_a_id_log_str = "ID: " + str(client_a_id) + " Session: " + str(session_id) + " IP: " + addr[0] + ":" + str(addr[1])

            logger.log(logging.DEBUG, "PRE-INIT Ver[%s] from client [%s].",
                       natneg_srv_protocol_ver, client_a_id_log_str)

            preinit_a = self.db.get_natneg_pre_init_client(session_id_raw, client_a_id_raw, True)
            client_bs = self.db.get_natneg_pre_init_client(session_id_raw, client_a_id_raw, False)

            # PRE-INIT response
            output_tmpl = bytearray(recv_data[0:8])
            output_tmpl[7] = 0x10 # Pre-init response
            output = None

            if preinit_a is None:
                # Create new pre-init client.
                created = self.db.update_natneg_pre_init_client(session_id_raw, client_a_id_raw, addr[0], addr[1], client_hoststate,
                                                                self.address[0], self.address[1], natneg_srv_protocol_ver, 0)
                if created == 1:
                    logger.log(logging.DEBUG, "Created new PRE-INIT Client [%s].", client_a_id_log_str)

                    # Refetch current client.
                    preinit_a = self.db.get_natneg_pre_init_client(session_id_raw, client_a_id_raw, True)
                else:
                    logger.log(logging.DEBUG, "Unable to create new PRE-INIT Client [%s].", client_a_id_log_str)

            dstr = "EXTRA DEBUG::::: Is PRE-INIT client returned from DB? ["
            if preinit_a is None:
                dstr += "NO]"
            else:
                dstr += "YES]"
            dstr += " Does Client ID = Data from Client? ["
            if utils.compare_bytes_in_bytearray(preinit_a["client_id"], client_a_id_raw):
                dstr += "YES]"
            else:
                dstr += "NO]"
            logger.log(logging.DEBUG, dstr)

            if preinit_a is not None and \
            "client_id" in preinit_a and utils.compare_bytes_in_bytearray(preinit_a["client_id"], client_a_id_raw) and \
            "session_id" in preinit_a and utils.compare_bytes_in_bytearray(preinit_a["session_id"], session_id_raw) and \
            "client_addr" in preinit_a and "client_port" in preinit_a and \
            "client_hoststate" in preinit_a and \
            "state" in preinit_a:

                # Check if we have a waiting status.
                if int(preinit_a["state"]) < 2:

                    # Check if we have a valid list of possible client bs.
                    if len(client_bs) > 0:
                        for preinit_b in client_bs:

                            # Check if we have been contacted by the remote client...
                            if preinit_b is not None and \
                               "session_id" in preinit_b and preinit_b["session_id"] == session_id_raw and \
                               "client_id" in preinit_b and preinit_b["client_id"] != client_a_id_raw and \
                               "client_addr" in preinit_b and "client_port" in preinit_b and \
                               len(str(preinit_b["client_addr"])) > 0 and \
                               len(str(preinit_b["client_port"])) > 0 and \
                               int(preinit_b["client_port"]) > 0 and \
                               "client_hoststate" in preinit_b and int(preinit_b["client_hoststate"]) != int(preinit_a["client_hoststate"]) and \
                               "natneg_server_addr" in preinit_b and "natneg_server_port" in preinit_b and \
                               "natneg_srv_protocol_ver" in preinit_b and \
                               "state" in preinit_b and int(preinit_b["state"]) == int(preinit_a["state"]) and \
                               int(preinit_a["state"]) == 0 or \
                               preinit_a["remote_match_id"] == preinit_b["client_id"]:
                                # Generate log string for client_b.
                                client_b_id = utils.get_int(preinit_b["client_id"], 0)
                                client_b_id_log_str = "ID: " + str(client_b_id) + " Session: " + str(session_id) + \
                                                      " IP: " + str(preinit_b["client_addr"]) + ":" + str(preinit_b["client_port"])

                                # Generate output packet for the connecting client.
                                # Note: The bytearray() call here is to perform a deep copy of the template buffer instead of a reference copy.
                                output = bytearray(output_tmpl)
                                output += preinit_a["session_id"]
                                output += utils.get_byte_from_bool(int(preinit_a["client_hoststate"]))
                                output += utils.get_byte_from_int((int(preinit_a["state"]) + 1), False)
                                output += preinit_b["client_id"]

                                # Send output packet to connecting client.
                                self.write_queue.put((output, (str(preinit_a["client_addr"]), int(preinit_a["client_port"]))))

                                # Update status for the connecting client.
                                self.db.update_natneg_pre_init_client(session_id_raw, client_a_id_raw, addr[0], addr[1], client_hoststate,
                                                                      self.address[0], self.address[1], natneg_srv_protocol_ver, int(preinit_a["state"]) + 1,
                                                                      preinit_b["client_id"])

                                logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK state [%s] response to client [%s].", natneg_srv_protocol_ver,
                                           (int(preinit_b["state"]) + 1), client_a_id_log_str)
                                logger.log(logging.DEBUG, utils.pretty_print_hex(output))

                                # Need to send notification to other client.
                                if str(preinit_b["natneg_server_addr"]) == self.address[0] and int(preinit_b["natneg_server_port"]) == self.address[1]:
                                    # Remote client is ours, so handle the notification.
                                    # Note: The bytearray() call here is to perform a deep copy of the template buffer instead of a reference copy.
                                    output = bytearray(output_tmpl)
                                    output += preinit_b["session_id"]
                                    output += utils.get_byte_from_bool(int(preinit_b["client_hoststate"]))
                                    output += utils.get_byte_from_int((int(preinit_b["state"]) + 1), False)
                                    output += preinit_a["client_id"]
                                    output[6] = utils.get_byte_from_int(int(preinit_b["natneg_srv_protocol_ver"])) # Fix the protocol version for the remote client.

                                    self.write_queue.put((output, (str(preinit_b["client_addr"]), int(preinit_b["client_port"]))))

                                    self.db.update_natneg_pre_init_client(session_id_raw, preinit_b["client_id"], preinit_b["client_addr"], preinit_b["client_port"],
                                                                          preinit_b["client_hoststate"],
                                                                          preinit_b["natneg_server_addr"], preinit_b["natneg_server_port"], preinit_b["natneg_srv_protocol_ver"],
                                                                          int(preinit_b["state"]) + 1, client_a_id_raw)

                                    logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK state [%s] response to client [%s].", \
                                               preinit_b["natneg_srv_protocol_ver"], (int(preinit_b["state"]) + 1), client_b_id_log_str)
                                    logger.log(logging.DEBUG, utils.pretty_print_hex(output))
                                else:
                                    # Remote client is handled by another server, tell it to handle the notification.
                                    output_server = bytearray([0x02])
                                    output_server += session_id_raw
                                    utils.write_bytes_to_bytearray(preinit_b["client_id"], output_server)
                                    output_server += client_a_id_raw
                                    output_server += bytearray(socket.htonl(int(preinit_b["state"]) + 1))

                                    self.server_write_queue.put((output_server, (str(preinit_b["natneg_server_addr"]), int(preinit_b["natneg_server_port"]))))
                                    logger.log(logging.DEBUG, "Sent server PRE-INIT ACK request to %s:%s for client [%s].",
                                               preinit_b["natneg_server_addr"], str(preinit_b["natneg_server_port"]), client_b_id_log_str)
                                    logger.log(logging.DEBUG, utils.pretty_print_hex(output_server))

                                # We've created the client pairing so break out of this loop.
                                break
                        if output is None:
                            # No other clients are ready yet.
                            # Note: The bytearray() call here is to perform a deep copy of the template buffer instead of a reference copy.
                            output = bytearray(output_tmpl)
                            output += preinit_a["session_id"]
                            output += utils.get_byte_from_bool(int(preinit_a["client_hoststate"]))
                            output += bytearray([0x00])
                            output += bytearray([0x00, 0x00, 0x00, 0x00])

                            logger.log(logging.DEBUG, "No other PRE-INIT clients are ready yet. " + \
                                       "PRE-INIT Client [%s] is the only known non-ready PRE-INIT client for session [%s].", client_a_id_log_str, str(session_id))

                            self.write_queue.put((output, (str(preinit_a["client_addr"]), int(preinit_a["client_port"]))))
                            logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK state [%s] response to client [%s].", natneg_srv_protocol_ver, str(0), client_a_id_log_str)
                            logger.log(logging.DEBUG, utils.pretty_print_hex(output))
                    else:
                        # No other clients have connected yet.
                        # Note: The bytearray() call here is to perform a deep copy of the template buffer instead of a reference copy.
                        output = bytearray(output_tmpl)
                        output += preinit_a["session_id"]
                        output += utils.get_byte_from_bool(int(preinit_a["client_hoststate"]))
                        output += bytearray([0x00])
                        output += bytearray([0x00, 0x00, 0x00, 0x00])

                        logger.log(logging.DEBUG, "No other PRE-INIT clients have connected yet. " + \
                                   "PRE-INIT Client [%s] is the only known PRE-INIT client for session [%s].", client_a_id_log_str, str(session_id))

                        self.write_queue.put((output, (str(preinit_a["client_addr"]), int(preinit_a["client_port"]))))
                        logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK state [%s] response to client [%s].", natneg_srv_protocol_ver, str(0), client_a_id_log_str)
                        logger.log(logging.DEBUG, utils.pretty_print_hex(output))
                else:
                    # Client is sending a ping...
                    # Note: The bytearray() call here is to perform a deep copy of the template buffer instead of a reference copy.
                    output = bytearray(output_tmpl)
                    output += preinit_a["session_id"]
                    output += utils.get_byte_from_bool(int(preinit_a["client_hoststate"]))
                    output += utils.get_byte_from_int(int(preinit_a["state"]), False)
                    output += bytearray([0x00, 0x00, 0x00, 0x00])

                    logger.log(logging.DEBUG, "Client [%s] sent a ping.", client_a_id_log_str)
                    self.write_queue.put((output, (str(preinit_a["client_addr"]), int(preinit_a["client_port"]))))
                    logger.log(logging.DEBUG, "Sent Ver[%s] PRE-INIT ACK state [%s] response to client [%s].", natneg_srv_protocol_ver, preinit_a["state"], client_a_id_log_str)
                    logger.log(logging.DEBUG, utils.pretty_print_hex(output))

            # Old implementation.
            #if session == 0:
                # What's the correct behavior when session == 0?
            #    output[13] = 2
            #elif session in self.natneg_preinit_session:
                # Should this be sent to both clients or just the one that connected most recently?
                # I can't tell from a one sided packet capture of Pokemon.
                # For the time being, send to both clients just in case.
            #    output[13] = 2
            #    self.write_queue.put((output, self.natneg_preinit_session[session]))

            #    output[12] = (1, 0)[output[12]] # Swap the index
            #    del self.natneg_preinit_session[session]
            #else:
            #    output[13] = 0
            #    self.natneg_preinit_session[session] = addr

            #self.write_queue.put((output, addr))

        else: # Was able to connect
            logger.log(logging.DEBUG, "Received unknown Ver[%s] command %02x from %s:%s...",
                       natneg_srv_protocol_ver, ord(recv_data[7]), addr[0], str(addr[1]))

    def get_server_info(self, gameid, session_id, client_id):
        server_info = None
        # Local note:
        # get_natneg_server() accesses a list of servers made in backend_server.py using the session_id as a key / index.
        # We basicly replicate it in natneg_sessions. The whole point is to get the ip address of the server.
        #
        servers = self.server_manager.get_natneg_server(session_id)._getvalue()

        if servers == None:
            return None

        console = False
        #ipstr = self.session_list[session_id][client_id]['addr'][0]
        client = self.db.get_natneg_session_client(session_id, gameid, client_id)
        if 'addr' in client:
            ipstr = client['addr']
            ip = str(utils.get_ip(bytearray([int(x) for x in ipstr.split('.')]), 0, console))
            console = not console

            server_info = next((s for s in servers if s['publicip'] == ip), None)

            if server_info == None:
                ip = str(utils.get_ip(bytearray([int(x) for x in ipstr.split('.')]), 0, console))

                server_info = next((s for s in servers if s['publicip'] == ip), None)

            return server_info

        return None

    def get_server_info_alt(self, gameid, session_id, client_id):
        console = False
        #ipstr = self.session_list[session_id][client_id]['addr'][0]
        client = self.db.get_natneg_session_client(session_id, gameid, client_id)
        if 'addr' in client and \
           'localaddr' in client and \
           'localport' in client and \
           'gameid' in client:
            ipstr = client['addr']
            local_ipstr = client['localaddr']
            ip = str(utils.get_ip(bytearray([int(x) for x in ipstr.split('.')]), 0, console))
            local_ip_le = utils.get_ip(bytearray([int(x) for x in local_ipstr.split('.')]), 0, False)
            local_ip_be = utils.get_ip(bytearray([int(x) for x in local_ipstr.split('.')]), 0, True)
            console = not console

            #serveraddr = self.server_manager.find_server_by_local_address(ip, self.session_list[session_id][client_id]['localaddr'], self.session_list[session_id][client_id]['gameid'])._getvalue()
            self.server_manager.find_server_by_local_address(ip, {client['localaddr'], client['localport'], local_ip_le, local_ip_be}, client['gameid'])._getvalue()

            if serveraddr == None:
                ip = str(utils.get_ip(bytearray([int(x) for x in ipstr.split('.')]), 0, console))

#                serveraddr = self.server_manager.find_server_by_local_address(ip, self.session_list[session_id][client_id]['localaddr'],
#                                                                                  self.session_list[session_id][client_id]['gameid'])._getvalue()
                serveraddr = self.server_manager.find_server_by_local_address(ip, {client['localaddr'], client['localport'], local_ip_le, local_ip_be}, client['gameid'])._getvalue()
            return serveraddr

        return None

if __name__ == "__main__":
    natneg_server = GameSpyNatNegServer()
    natneg_server.start()
