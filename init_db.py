#    DWC Network Server Emulator
#    Copyright (C) 2014 polaris-
#    Copyright (C) 2014 ToadKing
#    Copyright (C) 2014 AdmiralCurtiss
#    Copyright (C) 2019 EnergyCube
#    Copyright (C) 2023 redpolline
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys
import other.utils as utils

from gamespy.gs_database import Transaction as Transaction
from gamespy.gs_database import GamespyDatabase as GamespyDatabase
from gamespy.gs_database_nas import NASGamespyDatabase as NASGamespyDatabase
from gamespy.gs_database_natneg import NATNEGGamespyDatabase as NATNEGGamespyDatabase

# Config settings.
config_file = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), "./config.ini"))
config_sect = "GSDatabase"

# Logger settings
logger_name = "GSDatabase INIT"
logger = utils.create_logger(config_file, config_sect, logger_name)

def init_dbs():
    print "Init initial DB tables."
    db = GamespyDatabase("GamespyDatabase [%s]" % logger_name)
    db.initialize_database()
    db.close()
    del db

    print "Init NAS DB tables."
    db = NASGamespyDatabase("GamespyDatabase [%s]" % logger_name)
    db.initialize_database()
    db.close()
    del db

    print "Init NATNEG DB tables."
    db = NATNEGGamespyDatabase("GamespyDatabase [%s]" % logger_name)
    db.initialize_database()
    db.close()
    del db

# "Main" function below
print "This script will now attempt to initialize the required mysql tables."
init_dbs()
print "Script finished."
print "If no errors occurred, the database is now set up and you can run the master_server.py script.\n"
print "Otherwise:\n1) Check your config.ini located at [", config_file, "].\n2) Make sure the mysql database you are trying to initialize is empty.\n" \
      "3) That the database user has permission to CREATE / ALTER / DROP tables in the configured database.\n"
sys.exit(0)
